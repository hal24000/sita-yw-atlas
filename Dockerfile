FROM python:3.6.6
MAINTAINER HAL24K "docker@hal24k.com"
ENV PYTHONUNBUFFERED=0

RUN mkdir water_situational_awareness
WORKDIR water_situational_awareness

RUN apt-get update

# Copy install dependencies

RUN pip3 install --upgrade pip

RUN pip3 install numpy pylint pigar configobj>=5.0.6
RUN apt-get install -y scons
RUN curl https://baltocdn.com/helm/signing.asc | apt-key add -
RUN apt-get update --fix-missing
RUN apt-get install apt-transport-https --yes --fix-missing
RUN echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
RUN apt-get update --fix-missing

# Create work dir, and copy all files
WORKDIR /src
COPY ./src /src
RUN export $(cat .env | xargs)
#WORKDIR src
ENV WRKDIR /water_situational_awareness/src
# Copy requirements.txt and install dependencies top level
COPY requirements.txt /src


ARG PIP_TRUSTED_HOST=pipserver.dimension.ws
ARG PIP_EXTRA_INDEX_URL=https://hal_muser:Mkw5VxNDNxkOMYrIDqdAy645mLGvnqxW@pip.dimension.ws:443/
RUN pip3 install \
        --trusted-host ${PIP_TRUSTED_HOST} \
        --extra-index-url ${PIP_EXTRA_INDEX_URL} \
        -r requirements.txt
RUN pip3 uninstall -y bson pymongo \
    && pip install bson==0.5.7 \
    && pip install pymongo==3.7.2
RUN pip3 install git+https://github.com/benoitc/gunicorn.git
RUN pip3 uninstall -y azure-storage-blob \
    && pip3 install azure-storage-blob==2.1.0

RUN pip3 install lxml

RUN pip3 install dash==1.18.1
RUN pip3 install dash-core-components==1.14.1

# Copy app files to image

RUN rm -f conf/mongo_conf.txt \
    && touch conf/mongo_conf.txt \
    && echo "[mongoDB credentials]" > conf/mongo_conf.txt \
    && echo "username : " >> conf/mongo_conf.txt \
    && echo "password : " >> conf/mongo_conf.txt \
    && echo "database : cd_yorkshirewater" >> conf/mongo_conf.txt \
    && echo "uri: mongodb+srv://cd_yorkshirewater:cT2k1XbwS29CYYSgNvdblvvOOumyqMju@atlas-prod-pl-0.hldmf.mongodb.net" >> conf/mongo_conf.txt

ENV URL_PREFIX='/'

ENV PYTHONPATH=/src:/src/prod-water-sa-01
ENV SERVER_HOST='0.0.0.0'
ENV SERVER_PORT=8000
EXPOSE 8000

RUN ls -lcrth && which python && echo $PYTHON_PATH
ADD start.sh /
RUN chmod +x /start.sh
CMD ["/start.sh"]

