"""
1.1 in H.12 document.
"""
import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html
from visualization.plot_drawing.plotly_plots import draw_map
from dash.dependencies import Input, Output, State
from data.collections_object import ProjectCollections as prj_collections
from visualization.app_def import app
from data.pandas_cacher import BrainHolder

# installation_df =generate_map_data()
import logging
from data.project_loggers import dash_logger
logger = logging.getLogger(dash_logger)

risk_store_reader = BrainHolder(client_brains=['map-risk-reader'],
                                namespace="risk-nav")

content = html.Div(
    className='mt-0 mapPage__body',
    children=[
        html.Div(
            className='mapPage__map',
            children=[
                dcc.Graph(
                    id='mappage-map',
                    # figure=draw_map(prj_collections.installation_df),
                )
            ],
        ),
    ],
)

@app.callback(output=Output('nav-plant-dd', "value"),
              inputs=[Input("mappage-map", 'clickData')],
              state=[State('nav-selected-installation', 'data'),
                     State("nav-selected-page", 'data')])
def map_page_update_selected_inst_dd(clickData, current_selection, current_page):
    """
    
    Args:
        clickData:
        current_selection:
        current_page:

    Returns:

    """

    if clickData is None:
        raise dash.exceptions.PreventUpdate
    else:
        selected_inst = clickData.get('points')[0].get('customdata')[-1]
        logger.info(f"MAP CLICK: {selected_inst} - {current_page}")
        return selected_inst


@app.callback(Output('mappage-map', 'figure'),
              [Input('nav-risk-query', 'data')])
def redraw_map(hashed_query):
    grouped_risks = risk_store_reader.get('map-risk-reader',hashed_query)
    grouped_risks = grouped_risks.groupby('site_name')['risk'].max().reset_index()

    output_df = pd.merge(prj_collections.installation_df, grouped_risks,
                         on='site_name', how='left')
    output_df['risk'] = output_df['risk'].fillna(0.)

    return draw_map(output_df)