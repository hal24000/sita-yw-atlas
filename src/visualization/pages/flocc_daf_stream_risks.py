"""
flocc daf stream risks section 1.12 from H1.2 document
"""
import dash_core_components as dcc
import dash_html_components as html
from visualization.plot_drawing.plotly_plots import make_map_table, draw_map
from visualization.plot_drawing.table_styling import data_bars
import pandas as pd
import dash_table
from data.generate_data import generate_pump_data

# df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/2014_usa_states.csv')
# pump_data = generate_pump_data()

content = html.Div(
    html.Div(
        [
            # start pumps at risk col
            html.Div(
                [
                    html.H3("#Days Running by Stream"),
                    dash_table.DataTable(
                        id='summ-pumps-running-table',
                        columns=[{"name": i, "id": i} for i in
                                 pump_data.columns],
                        data=pump_data.to_dict("records"),
                        page_size=15,
                        style_as_list_view=True,
                        style_data_conditional=
                        data_bars(pump_data, 'days_running')
                        ,
                        style_header={
                            'backgroundColor': 'rgb(230, 230, 230)',
                            'fontWeight': 'bold'
                        },
                        style_table={'height': '95%'}
                    )],
                className="col-sm",
                style={'height': '100%', 'width': '20%'}
            ),
            # end pumps at risk col

            html.Div(
                [
                    html.Div([
                        html.Div([
                            html.Div([
                                html.Div('#Failures Last 7 Days', className='col'),
                                html.Div('0', className='col')
                            ], className='row')
                        ], className='col'),
                        html.Div([
                            html.Div([
                                html.Div('#Failures Last 28 Days', className='col'),
                                html.Div('0', className='col')
                            ], className='row')
                        ], className='col'),
                        html.Div([
                            html.Div([
                                html.Div('#Streams Unavail Last 7 Days', className='col'),
                                html.Div('0', className='col')
                            ], className='row')
                        ], className='col'),
                        html.Div([
                            html.Div([
                                html.Div('#Streams Unavail Last 28 Days', className='col'),
                                html.Div('0', className='col')
                            ], className='row')
                        ], className='col'),
                    ], className='row'),
                    make_map_table(df)
                ],
                className='col-md'
            ),
        ],
className='row flex-grow'),
className='container-fluid')
