"""
clarification-daf-streams adsorber risks, section 1.14 from document H1.2
"""
import dash_core_components as dcc
import dash_html_components as html

content = html.Div(
    className="clarification-daf-streams__page",
    children=[
        # First Column:
        html.Div(
            className="clarification-daf-streams__column-1 w-6 mr-16",
            children=[
                html.Div(
                    className="clarification-daf-streams__g1 w-6 h-nofilter-1-row-of-2 bg-white mb-16",
                    children=[
                        html.Div(
                            className="clarification-daf-streams__title title m-6",
                            children=["Days Running by Stream"],
                        ),
                        html.Div(
                            className="clarification-daf-streams__body",
                            children=[
                                html.Div(
                                    className="clarification-daf-streams__g1--body h-nofilter-1-row-of-2__body",
                                    # Height for table is 354px
                                ),
                            ],
                        )
                    ]
                ),
                html.Div(
                    className="clarification-daf-streams__g2 w-6 h-nofilter-1-row-of-2 bg-white",
                    children=[
                        html.Div(
                            className="clarification-daf-streams__title title m-6",
                            children=["DAF Streams with Current Performance Details"],
                        ),
                        html.Div(
                            className="clarification-daf-streams__body",
                            children=[
                                html.Div(
                                    className="clarification-daf-streams__g2--body h-nofilter-1-row-of-2__body",
                                    # Height for table is 354px
                                ),
                            ],
                        )
                    ]
                ),
            ],
        ),
        html.Div(
            className="clarification-daf-streams__column-2 w-6",
            children=[
                html.Div(
                    className="clarification-daf-streams__g3 w-6 h-filter mb-16",
                    children=[
                        html.Div(
                            className="clarification-daf-streams__g3--filter w-3 bg-white p-6 h-filter",
                            children=[
                                html.Div(
                                    className="clarification-daf-streams__g3--filter-title title",
                                    children=["Failures"],
                                ),
                                html.Div(
                                    className="clarification-daf-streams__g3--filter-body",
                                    children=[
                                        html.Div(
                                            className="button-f mr-16",
                                            children=[
                                                html.Div("7 Days", className="button-f__title"),
                                                html.Div("0", className="button-f__value")
                                            ]
                                        ),
                                        html.Div(
                                            className="button-f mr-10",
                                            children=[
                                                html.Div("28 Days", className="button-f__title"),
                                                html.Div("0", className="button-f__value")
                                            ]
                                        ),
                                    ],
                                )
                            ],
                        ),
                        html.Div(
                            className="clarification-daf-streams__g3--filter w-3 bg-white p-6 h-filter",
                            children=[
                                html.Div(
                                    className="clarification-daf-streams__g3--filter-title title",
                                    children=["Unavailable"],
                                ),
                                html.Div(
                                    className="clarification-daf-streams__g3--filter-body",
                                    children=[
                                        html.Div(
                                            className="button-f mr-16",
                                            children=[
                                                html.Div("7 Days", className="button-f__title"),
                                                html.Div("0", className="button-f__value")
                                            ]
                                        ),
                                        html.Div(
                                            className="button-f mr-10",
                                            children=[
                                                html.Div("28 Days", className="button-f__title"),
                                                html.Div("0", className="button-f__value")
                                            ]
                                        ),
                                    ],
                                )
                            ],
                        ),
                    ],
                ),
                html.Div(
                    className="clarification-daf-streams__g4 w-6 h-with-filter-1-row-of-1 bg-white",
                    children=[
                        html.Div(
                            className="clarification-daf-streams__title title m-6",
                            children=["DAF Streams Statistics Showing Recent History"],
                        ),
                        html.Div(
                            className="clarification-daf-streams__body",
                            children=[
                                dcc.Graph(
                                    className="clarification-daf-streams__g4--body h-with-filter-1-row-of-1__body",
                                    # Height for graph is 603px
                                ),
                            ],
                        ),
                    ],
                ),
            ]
        ),
    ]
)