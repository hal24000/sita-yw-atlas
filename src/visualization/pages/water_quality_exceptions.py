"""
water quality readings, section 1.11 from H1.2 document
"""


import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from visualization.app_def import app
from data.collections_object import ProjectCollections as prj_collections
from visualization.plot_drawing.plotly_plots import draw_bar_chart,\
    draw_pie_chart
from data.generate_data import generate_time_series
from visualization.pages import water_quality_filters
import plotly.express as px
import pandas as pd

import logging
from data.project_loggers import dash_logger

from data.pandas_cacher import BrainHolder
#hard coded need to make dynamic later
# fa_options = prj_collections.assets_df[assets_df['MED-WTW']

logger = logging.getLogger(dash_logger)

time_series = generate_time_series(no_devices=1,frequency='15T')

wq_store_reader = BrainHolder(client_brains=['wq-exc-barplot'],
                              namespace="water-quality")
# checks_by_issue_group = [30,40,10,23]
# check_labels = ['Issue-LA', 'Issue-HR','Issue-LR', 'Issue-HA']


# wq_exc_summary = generate_wq_exceptions_summary(6)

# wq_exc_summary = load_wq_exceptions_summary(plant='ACOMB', all_data=True)
# issues_groupped = wq_exc_summary.groupby('issue_type')['n_issues'].sum()
#
# # TODO: un-hardcode this
# issues_groupped = issues_groupped[['Issue-HR', 'Issue-HA', 'Issue-LA', 'Issue-LR']]
#
# check_labels= issues_groupped.index.values
# checks_by_issue_group = issues_groupped.values
#
# wq_exc_barplot = draw_bar_chart(wq_exc_summary, 'var','n_issues', args=dict(color='issue_type'))
#
# wq_exc_pie_fig = draw_pie_chart_traditional(checks_by_issue_group, check_labels, {})
# #TODO: add range feature

ts_args = {
    'draw_boundaries': {col: [time_series[col].min(), time_series[col].max()] for col in time_series.columns}
}
# wq_ts_fig = draw_time_series_plot(time_series, 'WQ Readings & Range', args=ts_args)

# table_readings_df = generate_water_quality_readings_tabledf(8,'Clarified Water Turbidity')
# table_readings = make_table(table_readings_df,10, "","WQ Readings and Range")


exceptions_content = html.Div(
    className="water-quality__exceptions",
    children=[
        dcc.Store(id='wq-exc-lookbackstore', storage_type="memory", data=0),
        html.Div(
            className="water-quality__exceptions--g1 w-8 bg-white mr-16 h-with-filter-1-row-of-1",
            children=[
                html.Div(
                        className="water-quality__exceptions--title title",
                        children=["Water Quality Exceptions by Metric"],
                ),
                html.Div(
                    className="water-quality__exceptions--sub-title",
                    id="wq-exc-barplot-subtitle",
                    children=[""]
                ),
                html.Div(
                    className="water-quality__exceptions--body h-with-filter-1-row-of-1__body",
                    children=[
                        dcc.Loading(
                            id='loading-wq-exc-barplot',
                            type=prj_collections.loading_animation,
                            children=[
                                dcc.Graph(id="wq-exc-barplot",
                                          config={'displayModeBar': False})#, figure=wq_exc_barplot)
                                ]
                        ),
                    ],
                )
            ],
        ),
        html.Div(
            className="water-quality__exceptions--g2 w-4 bg-white h-with-filter-1-row-of-1",
            children=[
                html.Div(
                        className="water-quality__exceptions--title",
                        children=["Water Quality Exceptions by Type"],
                ),
                html.Div(
                    className="water-quality__exceptions--sub-title",
                    id="wq-exc-pieplot-subtitle",
                    children=[""]
                ),
                html.Div(
                    className="water-quality__exceptions--body h-with-filter-1-row-of-1__body",
                    children=[
                        dcc.Loading(
                            id='loading-wq-exc-pieplot',
                            type=prj_collections.loading_animation,
                            children=[
                                dcc.Graph(id="wq-exc-pieplot",
                                          config={'displayModeBar': False})#, figure=wq_exc_pie_fig)
                            ]
                            ),
                    ],
                )
            ],
        ),
    ],
)

content = html.Div(
    className="water-quality-page__body",
    children=[
        # Row 1: Filters and other controls
        water_quality_filters.filters_content,
        # Row 2: Graph Contents
        html.Div(
            className="water-quality__graphs",
            id="wq-graph-contents-exc",
            children=exceptions_content
        ),
    ],
)

@app.callback(
    Output(component_id='wq-exc-barplot-subtitle', component_property='children'),
    [Input(component_id='nav-history-days', component_property='data')]
)
def update_exc_barplot_title(lookback_days: int):
    if lookback_days is None:
        return ''
    else:
        return 'Lookback period: {} days'.format(lookback_days)

@app.callback(
    Output(component_id='wq-exc-pieplot-subtitle', component_property='children'),
    [Input(component_id='nav-history-days', component_property='data')]
)
def update_exc_pieplot_title(lookback_days):
    if lookback_days is None:
        return ''
    else:
        return 'Lookback period: {} days'.format(lookback_days)

# @app.callback(output=Output("wq-exc-lookbackstore","data"),
#               inputs=[Input("wq-inp-upper","value")],
#               state=[State("wq-exc-lookbackstore","data")])
# def update_lookback_ceiling(value, current_value):
#     """
#     TODO: include upper limit
#     """
#
#     return max(value, current_value)


@app.callback(output=[Output("wq-exc-barplot", "figure"),
                      Output("wq-exc-pieplot", "figure")],
              inputs=[Input("wq-amber-tablestore", "data"),
                      Input("wq-dd-metrics", "value")])
def update_exception_charts(input_data, chosen_metrics):
    """
    Draw the charts with the metrics.
    """

    # input_df = pd.DataFrame(input_data)
    # print(input_data)
    input_df = wq_store_reader.get('wq-exc-barplot', input_data)
    # print(len(input_df))
    # print(input_df)
    if input_df.empty:
        # if empty before filtering
        return px.bar(), px.pie()

    input_df = input_df[input_df['tag_name'].map(lambda x: x in chosen_metrics)]
    # logger.info(input_df)
    if input_df.empty:
        # empty after filtering
        return px.bar(), px.pie()

    input_df.rename(columns={'low_amber': 'Falling',
                             'high_amber': 'Rising'}, inplace=True)

    updated_barplot = draw_bar_chart(input_df, x='metric', y=['Falling', 'Rising'],
                                     args=dict(color_discrete_map={'Falling': 'blue',
                                                                   'Rising': 'orange'},
                                               labels={'metric': 'Metric'},
                                               height=603)
                                     )

    updated_barplot.update_layout(legend={'traceorder': 'reversed'},
                                  legend_title_text='High ROC Type')
    updated_barplot.update_yaxes(title_text='Counts')
    updated_barplot.update_traces(hovertemplate='High ROC Type: Falling<br>Metric: %{x}<br>Occurences: %{y}<extra></extra>',
                                  selector={'name': 'Falling'})
    updated_barplot.update_traces(hovertemplate='High ROC Type: Rising<br>Metric: %{x}<br>Occurences: %{y}<extra></extra>',
                                  selector={'name': 'Rising'})

    cumulative_ambers = pd.DataFrame(input_df[['Falling', 'Rising']].sum()).reset_index()
    cumulative_ambers.columns = ['Issue Type', 'Issue Count']

    updated_pie_chart = draw_pie_chart(cumulative_ambers, x='Issue Type', y='Issue Count',
                                       args=dict(color=['Falling', 'Rising'],
                                       color_discrete_map={'Falling': 'blue',
                                                             'Rising': 'orange'},
                                       hole=0.))
    updated_pie_chart.update_layout(legend_title_text='High ROC Type',
                                    margin = dict(l=0, r=0, t=50, b=0, pad=0))

    updated_pie_chart.update_traces(
        hovertemplate='High ROC Type: %{label}<br>Occurences: %{value}<extra></extra>'
    )

    return updated_barplot, updated_pie_chart
