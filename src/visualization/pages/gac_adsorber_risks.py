"""
gac adsorber risks, section 1.14 from document H1.2
"""
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from visualization.app_def import app
from data.summary_utils import count_equip_failures,count_equip_unavailable,count_equip_status, get_color_button
from data.generate_data import generate_exc_avail, generate_exc_avail
from visualization.plot_drawing.plotly_plots import make_table, draw_pie_chart_donut,\
    draw_bar_chart, make_proper_datetime_formatting
from data.summary_utils import query_tags_conditional,get_equipment_list_local
from data.collections_object import ProjectCollections
import numpy as np
import pandas as pd
import logging
from datetime import timedelta
import plotly.graph_objects as go
from data.project_loggers import dash_logger
logger = logging.getLogger(dash_logger)


content = html.Div(
    className="gac__page",
    children=[
            # First Column:
        html.Div(
            className="gac__column-1 mr-16",
            children=[
                html.Div(
                    className="gac__g1 w-6 h-nofilter-1-row-of-2 bg-white mb-16",
                    children=[
                        html.Div(
                            className="gac__title title m-6",
                            children=["Days Running by Adsorber"],
                        ),
                        html.Div(
                            className="gac__body",
                            children=[
                                dcc.Loading(
                                    id='loading-gac-daysrunning-graph',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        dcc.Graph(
                                            className="gac__g1--body  h-nofilter-1-row-of-2__body",
                                            id='gac-daysrunning-graph'
                                        ),
                                    ]
                                )
                            ],
                        )
                    ]
                ),
                html.Div(
                    className="gac__g2 w-6 h-nofilter-1-row-of-2 bg-white",
                    children=[
                        html.Div(
                            className="gac__title title m-6",
                            children=["Adsorbers with Current Performance Details"],
                        ),
                        html.Div(
                            className="gac__body",
                            children=[
                                dcc.Loading(
                                    id='loading-gac-tablediv-current',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        html.Div(
                                            className="gac__g2--body h-nofilter-1-row-of-2__body",
                                            id='gac-tablediv-current'
                                        )
                                    ]
                                ),
                            ],
                        )
                    ]
                ),
            ],
        ),
        html.Div(
            className="gac__column-2",
            children=[
                html.Div(
                    className="gac__g3 w-6 h-filter mb-16",
                    children=[
                        html.Div(
                            className="gac__g3--filter w-3 h-filter mr-16 bg-white",
                            children=[
                                html.Div(
                                    className="gac__g3--filter-title p-6 title",
                                    children=["Failures"],
                                ),
                                html.Div(
                                    className="gac__g3--filter-body",
                                    children=[
                                        html.Div(
                                            className="button-f mr-16", id='gac-bttn-failures-week',
                                            children=[
                                                html.Div("7 Days", className="button-f__title"),
                                                dcc.Loading(
                                                    id='loading-gac-bttn-failures-week-content',
                                                    type=ProjectCollections.loading_animation,
                                                    children=[
                                                    html.Div("0", id='gac-bttn-failures-week-content', className="button-f__value")
                                                        ]),
                                            ]
                                        ),
                                        html.Div(
                                            className="button-f mr-10", id='gac-bttn-failures-month',
                                            children=[
                                                html.Div("28 Days", className="button-f__title"),
                                                dcc.Loading(
                                                    id='loading-gac-bttn-failures-month-content',
                                                    type=ProjectCollections.loading_animation,
                                                    children=[
                                                        html.Div("0", id='gac-bttn-failures-month-content', className="button-f__value")
                                                        ]),
                                            ]
                                        ),
                                    ],
                                )
                            ],
                        ),
                        html.Div(
                            className="gac__g3--filter w-3 bg-white p-6",
                            children=[
                                html.Div(
                                    className="gac__g3--filter-title title",
                                    children=["Unavailable"],
                                ),
                                html.Div(
                                    className="gac__g3--filter-body",
                                    children=[
                                        html.Div(
                                            className="button-f mr-16", id= 'gac-bttn-unavail-week',
                                            children=[
                                                html.Div("7 Days", className="button-f__title"),
                                                dcc.Loading(
                                                    id='loading-gac-bttn-unavail-week-content',
                                                    type=ProjectCollections.loading_animation,
                                                    children=[
                                                        html.Div("0", id='gac-bttn-unavail-week-content', className="button-f__value")
                                                    ]),
                                            ]
                                        ),
                                        html.Div(
                                            className="button-f mr-10",  id='gac-bttn-unavail-month',
                                            children=[
                                                html.Div("28 Days", className="button-f__title"),
                                                dcc.Loading(
                                                    id='loading-gac-bttn-unavail-month-content',
                                                    type=ProjectCollections.loading_animation,
                                                    children=[
                                                        html.Div("0", id='gac-bttn-unavail-month-content', className="button-f__value")
                                                    ]),
                                            ]
                                        ),
                                    ],
                                )
                            ],
                        ),
                    ],
                ),
                html.Div(
                    className="gac__g4 w-6 h-with-filter-1-row-of-1 bg-white",
                    children=[
                        html.Div(
                            className="gac__title title p-6",
                            children=["Adsorbers Statistics Showing Recent History"],
                        ),
                        dcc.Loading(
                            id='loading-gac-datastore',
                            type=ProjectCollections.loading_animation,
                            children=[
                                dcc.Store(id='gac-datastore-recent', storage_type='memory'),
                                dcc.Store(id='gac-datastore-current', storage_type='memory'),
                            ]
                        ),

                        html.Div(
                            className="gac__body h-with-filter-1-row-of-1__body",
                            children=[
                                dcc.Loading(
                                    id='loading-gac-tablediv-recent',
                                    type=ProjectCollections.loading_animation,
                                    children=[
                                        html.Div(
                                            className="gac__g4--body h-with-filter-1-row-of-1__body",
                                            id='gac-tablediv-recent'
                                        )]),
                            ],
                        ),
                    ],
                ),
            ]
        ),
    ]
)




@app.callback(
        output=[Output('gac-bttn-failures-month-content', 'children'),
                Output('gac-bttn-failures-month', 'style')],
        inputs=[
                Input('nav-datetime', 'data'),
                Input('nav-selected-installation', 'data')
        ],
        state=[State('gac-bttn-failures-month','style')]
)
def update_gac_failures_monthly(current_datetime, selected_installation ,style):
    tag_subcat_filter = ("Status", "Fail Status")
    search_terms = ["Failed", "FAILED"]
    current_datetime = pd.to_datetime(current_datetime)

    gac_adsorbers = ProjectCollections.gac_tags
    n = count_equip_status(site=selected_installation,
                           time_span=28,
                           reference_time=current_datetime,
                           tag_subcat_filter=tag_subcat_filter,
                           search_terms=search_terms,
                           equip_list=gac_adsorbers)
    color = get_color_button(n, 'weekly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
        output=[Output('gac-bttn-failures-week-content', 'children'),
                Output('gac-bttn-failures-week', 'style')],
        inputs=[
                Input('nav-datetime', 'data'),
                Input('nav-selected-installation', 'data')
        ],
        state=[State('gac-bttn-failures-week', 'style')]

)
def update_gac_failures_weekly(current_datetime, selected_installation,style):

    tag_subcat_filter = ("Status", "Fail Status")
    search_terms = ["Failed", "FAILED"]
    current_datetime = pd.to_datetime(current_datetime)

    gac_adsorbers = ProjectCollections.gac_tags
    n = count_equip_status(site=selected_installation,
                           time_span=7,
                           reference_time=current_datetime,
                           tag_subcat_filter=tag_subcat_filter,
                           search_terms=search_terms,
                           equip_list=gac_adsorbers)

    color = get_color_button(n, 'weekly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style

@app.callback(
        output=[Output('gac-bttn-unavail-month-content', 'children'),
                Output('gac-bttn-unavail-month', 'style')],
        inputs=[
                Input('nav-datetime', 'data'),
                Input('nav-selected-installation', 'data')

        ],
        state=[State('gac-bttn-unavail-month', 'style')]
)
def update_gac_unavail_monthly(current_datetime, selected_installation,style):
    tag_subcat_filter = "AvailStatus"
    search_terms = ["UNAVAILABLE", "UNAVAIL", "Unavail", "Unavailable"]
    current_datetime = pd.to_datetime(current_datetime)

    gac_adsorbers = ProjectCollections.gac_tags
    n = count_equip_status(site=selected_installation,
                           time_span=28,
                           reference_time=current_datetime,
                           tag_subcat_filter=tag_subcat_filter,
                           search_terms=search_terms,
                           equip_list=gac_adsorbers)

    color = get_color_button(n, 'monthly')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style


@app.callback(
        output=[Output('gac-bttn-unavail-week-content', 'children'),
                Output('gac-bttn-unavail-week', 'style')],

        inputs=[
                Input('nav-datetime', 'data'),
                Input('nav-selected-installation', 'data'),
        ],
        state=[State('gac-bttn-unavail-week', 'style')]
)
def update_gac_unavail_weekly(current_datetime, selected_installation,style):
    tag_subcat_filter = "AvailStatus"
    search_terms = ["UNAVAILABLE", "UNAVAIL", "Unavail", "Unavailable"]
    current_datetime = pd.to_datetime(current_datetime)

    gac_adsorbers = ProjectCollections.gac_tags
    n = count_equip_status(site=selected_installation,
                           time_span=7,
                           reference_time=current_datetime,
                           tag_subcat_filter=tag_subcat_filter,
                           search_terms=search_terms,
                           equip_list=gac_adsorbers)
    color = get_color_button(n, 'daily')
    if style is not None:
        style['background-color'] = color
    else:
        style = {'background-color': color}
    return n, style

@app.callback(
        output=Output('gac-datastore-current', 'data'),
        inputs=[
                Input("nav-datetime", 'data'),
                Input('nav-selected-installation', 'data')
        ],
)
def update_gac_data_current(current_datetime, selected_installation):
    gac_tags = ProjectCollections.gac_tags

    query = {'tag_name':{'$in':gac_tags},
             'site_name':selected_installation}

    skip_ ={'_id':0, 'fabricated_key':0}
    res = pd.DataFrame(list(ProjectCollections.series_and_alarms_coll_latest.find(query, skip_)))
    res = make_proper_datetime_formatting(res)

    return res.to_dict('records')


@app.callback(
        output=Output('gac-datastore-recent', 'data'),
        inputs=[
                Input("nav-datetime", 'data'),
                Input('nav-selected-installation', 'data')
        ],
)
def update_gac_data_recent(current_datetime, selected_installation):
    gac_tags = ProjectCollections.gac_tags

    current_datetime = pd.to_datetime(current_datetime)
    prev_datetime = current_datetime - timedelta(hours=24)
    query = {'tag_name':{'$in':gac_tags},
             'measurement_timestamp':{'$gte':prev_datetime,
                                      '$lte':current_datetime},
             'site_name':selected_installation}
    skip_ ={'_id':0, 'fabricated_key':0}

    res = pd.DataFrame(list(ProjectCollections.series_and_alarms_coll.find(query,skip_)))
    res.fillna('nan', inplace=True)
    res = make_proper_datetime_formatting(res)

    return res.to_dict('records')


@app.callback(
    output=Output('gac-table-recent', 'data'),
    inputs=[Input('gac-table-recent', "page_current"),
     Input('gac-table-recent', "page_size")],
    state=[State('gac-datastore-recent', 'data')])
def exc_avail_navigate_gactable_recent(page_current,page_size, current_data):
    return current_data[
        page_current*page_size:(page_current+ 1)*page_size
    ]


@app.callback(
    output=Output('gac-table-current', 'data'),
    inputs=[Input('gac-table-current', "page_current"),
     Input('gac-table-current', "page_size")],
    state=[State('gac-datastore-current', 'data')])
def exc_avail_navigate_gactable_current(page_current,page_size, current_data):
    return current_data[
        page_current*page_size:(page_current+ 1)*page_size
    ]

@app.callback(output=Output('gac-tablediv-current', "children"),
              inputs=[
                      Input('gac-datastore-current', 'data'),
              ])
def update_gac_datatable_current(df_store):
    df_store = pd.DataFrame(df_store)

    if len(df_store) == 0:
        return make_table(pd.DataFrame(),10,'gac-table-current', 'No data found')

    df_store['tag_name'] = df_store['tag_name'].apply(lambda x: x.split(":")[1])

    table = make_table(df_store,8,'gac-table-current',{'height': '294px'})
    return table


@app.callback(output=Output('gac-tablediv-recent', "children"),
              inputs=[
                      Input('gac-datastore-recent', 'data'),
              ])
def update_gac_datatable_recent(df_store):
    df_store = pd.DataFrame(df_store)

    if len(df_store) == 0:
        # return make_table(pd.DataFrame(),10,'gac-table-recent', 'No data found')
        return html.Div(["No data found"])

    df_store['tag_name'] = df_store['tag_name'].apply(lambda x: x.split(":")[1])

    table = make_table(df_store,10,'gac-table-recent', None)
    return table


@app.callback(output=Output("gac-daysrunning-graph", "graph"),
              inputs=[Input('nav-datetime', 'data'),
                      Input('nav-selected-installation', 'data')])
def update_days_running_graph(current_datetime, selected_installation):

    gac_tags = ProjectCollections.gac_tags

    query = {'tag_name':{'$in':gac_tags},
             'site_name':selected_installation}

    skip_ ={'_id':0, 'fabricated_key':0}

    res = list(ProjectCollections.series_and_alarms_coll_latest.find(query, skip_))

    return go.Figure()