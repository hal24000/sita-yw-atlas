"""
Functions to manipulate dash components
Copyright 2020 HAL24K
"""

import dash_bootstrap_components as dbc


def get_active_elements(dropdown: dbc.DropdownMenu) -> list:
    """
    Return a list of elements that are active in a dropdown.

    Args:
        dropdown (dbc.DropdownMenu): dropdown menu in question

    Returns:
        list of element names that are active
    """

    active_elements = []

    for element in dropdown:
        if not isinstance(element.children, list):
            pass

        if element.children[0][0].children.on is True:
            active_elements.append(element.children[0][1].children)

    return active_elements


def get_all_controllable_elements(dropdown: dbc.DropdownMenu) -> list:
    """
    Return a list of all element names in a dropdown

    Args:
        dropdown (dbc.DropdownMenu): dropdown menu in question

    Returns:
        list of all element names
    """

    all_elements = []

    for element in dropdown:
        if not isinstance(element.children, list):
            pass

        all_elements.append(element.children[0][1].children)

    return all_elements
