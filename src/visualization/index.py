"""This page is the overview page. It contains the navigation bar and navigation
functionalities. It should import and navigate to the other pages.
"""
# this script will be run twice, once when the index page is imported into runserver.py and once
# when the app.run is called. the second time around the working dir will be the root of the project for some reason
# need to set the working directory to the 'WRKDIR' environment variable set in the runserver.py (project/src)
# so that all scripts assume they are under src/

import os

script_path = os.path.dirname(os.path.realpath(__file__))
os.environ['WRKDIR'] = script_path
os.chdir(os.environ['WRKDIR'])

import argparse
import traceback
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
import dash_html_components as html
from visualization.pages import (map_page, summary_page, nav_header, plc_data,plc_communication, filtration_rgf,
                    failure_exceptions,availability_exceptions, water_quality_exceptions,
                    water_quality_readings,plc_communication, plc_data, filtration_gac,
                    bearing_temperature_exceptions, motor_temperature_exceptions, clarification_daf_streams, filtration_rgf, none_execution_maintenance_plans,
                    schematic_drilldown)
from visualization.app_def import app
import logging
from os import environ

from data.project_loggers import dash_logger
## base index layout

logger = logging.getLogger(dash_logger)

app.layout = html.Div([
    dcc.Location(id='url', refresh=False, pathname='yorkshire-water'),
    dcc.Store(id='stored-session-url', storage_type='session'),
    nav_header.content, # the header div
    html.Div(id='page-content', children=[map_page.content], className="page"), #the main content
    #hidden storage elements
    # html.Div([html.Div(id="target-url"),
    #           html.Div(id="prev-url"),
    #           html.Button(id="prework-clicks"),
    #           html.Button(id="work-clicks"),
    #           dcc.Store(id="storage-page-summary", storage_type='session'),
    #           dcc.Store(id="storage-page-map", storage_type='session')], id="storage", hidden=True)
])

pages = {
    'map': map_page.content,
    'summary': summary_page.content,
    'comms-issues': plc_communication.content,
    'data-issues':plc_data.content,
    'motor-ot':None,
    "bearing-ot":None,
    # 'new-caching': new_caching.content,
    'availability-exceptions': availability_exceptions.content,
    'failure-exceptions': failure_exceptions.content,
    # 'flocc-daf-stream-risks': flocc_daf_stream_risks.content,
    # 'gac-adsorber-risks': gac_adsorber_risks.content,
    'rapid-gravity-filter-risks': filtration_rgf.content,
    'water-quality-exceptions': water_quality_exceptions.content,
    'water-quality-readings': water_quality_readings.content,
    'bearing-temperature-exceptions': bearing_temperature_exceptions.content,
    'motor-temperature-exceptions': motor_temperature_exceptions.content,
    'clarification-daf-streams': clarification_daf_streams.content,
    'filtration-rgf': filtration_rgf.content,
    'filtration-gac': filtration_gac.content,

    'plc-communication': plc_communication.content,
    'plc-data': plc_data.content,
    'none-execution-maintenance-plans': none_execution_maintenance_plans.content,
    'schematic-drilldown': schematic_drilldown.content,
}
@app.callback(
    Output('page-content', 'children'),
    [Input("url", "pathname")],
    [State('stored-session-url','data')]
)
def change_page( curr_url, stored_session_url):
    logger.info("Entered change page code")
    if curr_url == "/" or curr_url == None:
        if stored_session_url is not None:
            content = pages.get(stored_session_url, map_page.content)
            return content
        else:
            logger.info("setting to map page")
            return map_page.content

    else:
        content = pages.get(curr_url, map_page.content)
        return content

# if __name__ == "__main__":
#
#
#     if os.environ.get('PASSES', None):
#         os.environ['PASSES'] =  str(int(os.environ['PASSES']) + 1)
#     else:
#         os.environ['PASSES'] = "1"
#
#     logger.info("Pass {}".format(os.environ['PASSES']))
#
#     argparser = argparse.ArgumentParser()
#     argparser.add_argument("-local_mode", action="store_true",
#                            default=False, required=False)
#     argparser.add_argument("-remote_debugger", action="store_true",
#                            required=False, default=True)
#
#     args = argparser.parse_args()
#     HOST = environ.get('SERVER_HOST', 'localhost')
#
#     try:
#         PORT = int(environ.get('SERVER_PORT', '19888'))
#     except ValueError:
#         PORT = 19888
#     # run the server with a try catch, send the traceback via email to the specified account
#     try:
#         app.run_server(debug=True, host=HOST, port=PORT, use_reloader=False, processes=1,
#                        threaded=True)
#     except Exception as ex:
#         msg = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
#         logger.error(msg)