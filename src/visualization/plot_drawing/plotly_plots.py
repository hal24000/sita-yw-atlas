"""
All the plot drawing functions go here
"""

import pandas as pd

from plotly import graph_objects as go
import plotly.express as px
import copy
import dash_table
from dash_table.Format import Format
import numpy as np
from data.load_data import risk_thresholds, colors_risk_map, colors_rag,colors_risk
from data.project_loggers import dash_logger
from data.collections_object import ProjectCollections

import logging
logger = logging.getLogger(dash_logger)

layout = dict(
    margin=dict(
        l=30,
        r=30,
        b=20,
        t=40
    ),
    hovermode="closest",
    plot_bgcolor="#F9F9F9",
    paper_bgcolor="#F9F9F9",
    legend=dict(font=dict(size=10), orientation='h'),
)

def make_table(df, page_size, id, title, styledata_cond= None, style_header_cond=None,style_cell=None,
               numeric_columns=list(), decimals_displayed=3, custom_height_multiplier=30,
               page_action='custom', filter_action='none'):
    # page_size = 10
    page_count = max([len(df)// page_size + 1 if len(df) % page_size != 0 else len(df) // page_size, 1])
    height= custom_height_multiplier * (page_size + 1)#  + 60

    # identify datetime columns
    datetime_cols = list(df.dtypes[df.dtypes == 'datetime64[ns]'].index)

    for col in datetime_cols:
        df[col] = df[col].map(lambda x: x.strftime(ProjectCollections.dashboard_datetime_format))

    styledata_cond_default = [
                    {'if':              {'row_index': 'odd'},
                     'backgroundColor': 'rgb(248, 248, 248)'
                     },
                    # {'if':                      {
                    #         'filter_query': '{Reading} < {Lo}'
                    # },
                    #         'background_color': '#1f77b4',
                    #         'color':            'white'},
                    # {'if':                      {
                    #         'filter_query': '{Reading} > {Hi}'
                    # },
                    #         'background_color': '#ff7f0e',
                    #         'color':            'white'},

            ]
    style_cell_default = {'whiteSpace': 'normal', 'fontSize': 12}
    if style_cell is not None:
        style_cell_default = style_cell

    if styledata_cond is not None:
        styledata_cond_default += styledata_cond

    data_table_readings = dash_table.DataTable(
            id=id,
            style_table={'height':height, 'title':title, 'overflowY': 'auto'},
            columns=[{"name": i, "id": i} if i not in numeric_columns\
                     else {"name": i, "id": i, "type": "numeric",
                           "format": Format(nully='N/A', precision=decimals_displayed)}\
                     for i in df.columns],
            data=df.to_dict("records"),
            # fixed_rows={'headers': True},
            # fixed_columns={'headers': True, 'data': 0},
            virtualization=True,
            # style_table={'height': 30 * (page_size + 1) + 60, 'overflowY': 'auto'},
            style_as_list_view=False,
            # css={'min-height':200},
            style_cell=style_cell_default,
            style_data_conditional=styledata_cond_default,
            style_header_conditional=style_header_cond,
            style_header={
                    'backgroundColor': 'rgb(230, 230, 230)',
                    'fontWeight':      'bold',
                    # 'min_height':200,

            },
            page_current=0,
            page_size=page_size,
            page_count = page_count,
            page_action=page_action,
            filter_action=filter_action,
    )

    logger.info('make_table: table is out')
    logger.info('Showing {} rows.'.format(len(df)))

    return data_table_readings



def make_map_table(df):
    table = dash_table.DataTable(
        id='map-installations-table',
        columns=[{"name": i, "id": i} for i in df.columns],
        data=df.to_dict("records"),
        page_size=15,
        style_as_list_view=True,
        style_header={
            'backgroundColor': 'rgb(230, 230, 230)',
            'fontWeight': 'bold'
        },
        style_table={'height': '95%'}
    )
    return table
def draw_map(df):
    """
    plots a map with scattered points. df must contain lat and lon columns
    """

    if 'risk' not in df:
        df['risk'] = [np.random.rand() for i in range(len(df))]

    def get_color(x):
        for key, value in risk_thresholds.items():
            # print(key)
            if x <= key[1] and x >= key[0]:
                return value

    df['color_code'] = df['risk'].apply(lambda x: get_color(x))
    df['risk'] = np.round(df.risk, 3)
    color_to_word = {'red':'HIGH RISK', 'neutral':'OK', 'amber':'WARNING'}
    df['word_warning'] = df['color_code'].apply(lambda x: color_to_word[x])

    fig = px.scatter_mapbox(df, lat="latitude_1", lon="longitude_1",custom_data=['risk','site_name'],
                            color='color_code', color_discrete_map=colors_risk_map, zoom=9)

    fig.update_layout(mapbox_style="white-bg",
                      # clickmode="event",
                      showlegend=False,
                      # autosize=True,
                      # height=800,
                      coloraxis_showscale=False,
                      margin={"r": 0, "t": 0, "l": 0, "b": 0},
                            mapbox_layers=[
                                {
                                    "below": 'traces',
                                    "sourcetype": "raster",
                                    # 'color':'grey',
                                    "source": [
                                        #"https://c.tile.osm.org/{z}/{x}/{y}.png", 
                                        #"https://tile.dimension.ws/styles/osm-bright/{z}/{x}/{y}.png"
                                        "https://tile.cloud.dimension.ws/styles/osm-bright/{z}/{x}/{y}.png"
                                       
                                    ]
                                }]
                      )

    fig.update_traces(selector={'legendgroup': 'amber'}, name='Warning')
    fig.update_traces(selector={'legendgroup': 'red'}, name='High Risk')
    fig.update_traces(selector={'legendgroup': 'neutral'}, name='OK')

    fig.update_traces(
            marker={'size': 30},
            hovertext=df['word_warning'],
            hovertemplate="<br>".join([
                    'Site: %{customdata[1]}',
                    # "Latitude: %{lat}",
                    # "Longitude: %{lon}",
                    "Risk: %{customdata[0]}",
            ])
    )
    return fig

def draw_time_series_plot(time_series,title="",
                         colors=['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00'],
                          args={}):
    """
    Draws a time series plot, cycles throug the given colours.

    Args:
        time_series (pd.DataFarme): the time series dataframe
        colors (list(string)): a list of colors to use, 1 for each column
    Returns:
        fig (go.figure()) : the time series figure
    """
    data = []
    ts_layout = copy.deepcopy(layout)
    ts_layout['title'] = title
    for i, col in enumerate(time_series.columns):
        # print(col.split(":")[1])
        print(col)
        data.append(go.Scatter(x=time_series.index,
                               y= time_series[col],
                               name=col.split(":")[1],
                               mode="lines+markers",
                               line={'shape': 'spline',
                                    'smoothing': 1.,
                                    'width': 1,
                                    'color': colors[i]
                                   },
                               marker={'symbol': 'diamond-open'}
                               ))


        if 'draw_boundaries' in args:
            boundaries_ = args['draw_boundaries'].get(col, [])
            if boundaries_:
                data.append(go.Scatter(x=time_series.index,
                                       y=[boundaries_[0] for z in range(len(time_series))],
                                       name=col+"_lower",
                                       mode="lines+markers",
                                       line={'shape': 'spline',
                                             'smoothing': 1.,
                                             'width': 1,
                                             'color': 'blue'
                                             },
                                       marker={'symbol': 'diamond-open'}
                                   ))
                data.append(go.Scatter(x=time_series.index,
                                       y=[boundaries_[1] for z in range(len(time_series))],
                                       name=col + "_upper",
                                       mode="lines+markers",
                                       line={'shape': 'spline',
                                             'smoothing': 1.,
                                             'width': 1,
                                             'color': 'red'
                                             },
                                       marker={'symbol': 'diamond-open'}
                                       ))
    fig = go.Figure(data, layout)

    return fig


def draw_bar_chart(df, x, y, args={}, layout_update= {}):
    """
    Draws a bar chart

    Args:
        x (string): the x axis column
        y (string): the y axis column
    Returns:
        bar_chart_figure (go.Figure): the bar chart figure
    """
    bar_chart_figure = px.bar(df, x=x, y=y, **args)
    bar_chart_figure.update_layout(coloraxis_showscale=False, hovermode='x', **layout_update)
    return bar_chart_figure


def draw_pie_chart(df, x, y, args={}, layout_update= {}):
    """
    Draws a pie chart

    Args:
        x (string): the x axis column
        y (string): the y axis column
    Returns:
        bar_chart_figure (go.Figure): the bar chart figure
    """
    pie_chart_figure = px.pie(df, names=x, values=y, **args)
    pie_chart_figure.update_layout(coloraxis_showscale=False, **layout_update)

    px.pie()
    return pie_chart_figure


def draw_pie_chart_traditional(counts,labels, args):
    title= args.get('title', "")

    pie_layout = copy.deepcopy(layout)
    pie_layout['title'] = title

    data = [{
        'type': 'pie',
        'labels': labels,
        'values': counts,
        'hole': 0.5,
        'hoverinfo': "none",
        'textinfo': 'percent'
    }]

    pie_layout['showlegend'] = True

    fig = go.Figure(data, pie_layout)
    fig.update_layout(**args)

    return fig


def draw_pie_chart_donut(counts, labels, pct=True, args={}):
    """
    draws a pie chart with 2 values (val 1 and total)

    Args:
        counts_data (pd.DataFrame) : a dataframe with the data
        col_a (string): the first column to count
        col_b (string): the second column to count
        title (string): the title of the plot
    Returns:
        fig (go.Figure): the go figure
    """

    title= args.get('title', "")

    pie_layout = copy.deepcopy(layout)
    pie_layout['title'] = title

    if pct:
        pie_layout['annotations'] = [{'text': '{0:.0f}%'.format(100 * counts[0]/ counts[1]),
                                  'x': 0.5, 'y': 0.5, 'font': {'size': 30}, 'showarrow': False}]
    else:
        pie_layout['annotations'] = [{'text': '{}'.format(counts[0]),
                                  'x': 0.5, 'y': 0.5, 'font': {'size': 30}, 'showarrow': False}]

    pie_layout['showlegend'] = False
    data = [{
        'type': 'pie',
        'labels': labels,
        'values': counts,
        'hole': 0.5,
        'hoverinfo': "label+value",
        'textinfo': 'none'
    }]

    fig = go.Figure(data, pie_layout)
    fig.update_layout(showlegend=True)
    return fig


def make_proper_datetime_formatting(df: pd.DataFrame,
                                    dt_format: str = ProjectCollections.dashboard_datetime_format) -> pd.DataFrame:
    """
    Ensure that dataframe has the proper datetime formatting by turning it into the appropraite string
    """

    internal_df = df.copy()

    # identify datetime columns
    datetime_cols = list(internal_df.dtypes[internal_df.dtypes == 'datetime64[ns]'].index)

    for col in datetime_cols:
        internal_df[col] = internal_df[col].map(lambda x: x.strftime(dt_format))

    return internal_df
