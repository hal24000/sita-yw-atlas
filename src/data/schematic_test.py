
import dash_core_components as dcc
from dash.dependencies import Input, Output, State, ClientsideFunction
import dash
import dash_html_components as html
from datetime import datetime as dt
from bs4 import BeautifulSoup as soup
import logging
from project_loggers import dash_logger
import plotly.graph_objects as go
from wand.image import Image
import base64
logger = logging.getLogger(dash_logger)

schematics_file = "assets/schematic_large.svg"
schematics_rb = open(schematics_file,'r').read()
svg_mem_soup = soup(schematics_rb, 'lxml-xml')

app = dash.Dash(
    __name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}]
)
server = app.server

app.layout= html.Div(
    [
    dcc.Graph(id='schematic-graph'),
    html.Button(id='high-bttn', value='High'),
    html.Button(id='low-bttn',value='Low')
    ]
)

# Create global chart template
mapbox_access_token = "pk.eyJ1IjoiamFja2x1byIsImEiOiJjajNlcnh3MzEwMHZtMzNueGw3NWw5ZXF5In0.fk8k06T96Ml9CLGgKmk81w"
#mapbox layout
# 'lon':-1.123663130590768, 'lat': 53.95018617358313
layout = dict(
    autosize=True,
    automargin=True,
    margin=dict(l=30, r=30, b=20, t=40),
    hovermode="closest",
    plot_bgcolor="#F9F9F9",
    paper_bgcolor="#F9F9F9",
    legend=dict(font=dict(size=10), orientation="h"),
    # title="Satellite Overview of water treatment plants",
    mapbox=dict(
        accesstoken=mapbox_access_token,
        style="light",
        center=dict(lon=-1.10366, lat=53.9501),
        zoom=11,
    ),
)

@app.callback(
    Output('schematic-graph', 'figure'),
    [Input('high-bttn','n_clicks'),
     Input('low-bttn','n_clicks')]
)
def draw_schematic(n_clicks_high, n_clicks_low):
    fig =  go.Figure()
    svg_mem = str(svg_mem_soup)
    svg_mem = svg_mem.encode('utf-8')
    with Image(blob=svg_mem, format="svg" ) as image:
        png_image = image.make_blob("png",)
    base64_png = base64.b64encode(png_image)
    filename = 'img.png'  # I assume you have a way of picking unique filenames
    with open(filename, 'wb') as f:
        f.write(png_image)
    # print(base64_png)
    scale_factor= 2
    fig.add_layout_image(
        go.layout.Image(
            sizex=1,
            # y=img_height * scale_factor,
            sizey=1,
            xref="x",
            yref="y",
            opacity=1.0,
            layer="below",
            sizing="stretch",
            source='data:image/png;base64,{}'.format(base64_png.decode()))
    )
    return fig


if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--debug', action="store_true")
    args = parser.parse_args()


    print(args.debug)
    if args.debug:
        logger.setLevel('DEBUG')
    else:
        logger.setLevel('INFO')

    app.run_server(host='0.0.0.0', debug=args.debug,)# dev_tools_hot_reload = False
