import numpy as np
from data.summary_utils import query_tags_conditional
from data.gac_collections import GACCollections

def preproc_bwdue_acomb(df):
    due = df.loc[(df.measurement_value >0) &
                 (df.service_status =='filtering')]
    return len(due)

def assign_filtering_stat_elv(df,reference_time):
    df['service_status'] = 'filtering'
    df.loc[df['measurement_value'] == 'OFF', 'service_status']= 'oos'
    bw_tags = GACCollections.individual_tags_elvington['bw stepno']['tag_name'].tolist()
    bwsteps = query_tags_conditional(bw_tags,reference_time=reference_time,latest_only=True)

    if len(bwsteps)>0:
        bwsteps = bwsteps.sort_values(by='tag_name').reset_index(drop=True)
        df = df.sort_values(by='tag_name').reset_index(drop=True)
        df.loc[bwsteps.measurement_value >0, 'service_status']='washing'

    return df['service_status']

def assign_filtering_stat_acomb(df, reference_time):
    df['service_status'] = 'filtering'
    df.loc[df['measurement_value'].isin([0,16]), 'service_status']= 'oos'
    df.loc[df['measurement_value'].isin(np.arange(2,13)), 'service_status']= 'washing'

    # df.set_index('tag_name', inplace=True)

    return df['service_status']

def assign_filtering_stat_huby(df, reference_time):
    df['service_status'] = 'filtering'
    #df contains service status
    df = df.sort_values(by='tag_name') #ensure theyre sorted
    df.loc[df['measurement_value'] == 'OUT OF SERVICE', 'service_status']= 'oos'

    #also check which ones backwashing
    gac_washing_tags = ['HUBYWTS1:GAC_CURRENT_WASHING']
    curr_washing_df = query_tags_conditional(gac_washing_tags,
                                             reference_time=reference_time,
                                             latest_only=True)
    if len(curr_washing_df)>0:
        if int(curr_washing_df['measurement_value']) > 0:
            df.loc[int(curr_washing_df['measurement_value'])-1, 'service_status'] ='washing'
    # df.set_index('tag_name', inplace=True)

    return df['service_status']





def preproc_bwdue_huby(df):
    due = df.loc[(df.measurement_value > (72-1)) &
                 (df.service_status =='filtering')]
    return len(due)

def preproc_bwdue_elv(df):
    due = df.loc[(df.measurement_value > (78-1)) &
                 (df.service_status =='filtering')]
    return len(due)

def preproc_bwreq_huby(df):
    req = df.loc[(df.measurement_value.str.lower() == 'high') &
                 (df.service_status =='filtering')]
    return req

def preproc_bwreq_acomb(df):
    req = df.loc[(df.measurement_value.str.lower() == 'high') &
                 (df.service_status == 'filtering')]
    return req

def preproc_bwreq_elv(df):
    req = df.loc[(df.measurement_value> 0.2) &
                 (df.service_status =='filtering')]
    return req

def preproc_bw_current_acomb(df):
    bwing = df.loc[df.measurement_value.isin(np.arange(3,13))]
    return len(bwing)

def preproc_bw_current_huby(df):
    bwing = df['measurement_value'].values.squeeze()
    return bwing

def preproc_bw_current_elv(df):
    bwing = df.loc[df['measurement_value']>0]
    return len(bwing)

def preproc_exc_queue_acomb(df):
    exceeding = df.loc[(df['measurement_value'].str.upper() == 'IN EXCESS') &
                 (df.service_status =='filtering')]
    return len(exceeding)


def preproc_outflow_elv(val):
    return val*0.0864


def postprocess_df_huby(df, reference_time):
    df['BW StepNo'] = np.nan

    step_active_tags = ["HUBYWTS1:GAC_WASH_STEP_{}_ACTIVE".format(i) for i in range(1, 10)]
    gac_washing_tags = ['HUBYWTS1:GAC_CURRENT_WASHING']

    currently_washing_df = query_tags_conditional(gac_washing_tags, latest_only=True, reference_time=reference_time)
    currently_washing = currently_washing_df['measurement_value']

    if currently_washing != 0:
        current_wash_step = query_tags_conditional(step_active_tags, latest_only=True, reference_time=reference_time)
        step_no_active = current_wash_step.loc[current_wash_step.measurement_value == "Active"].index.values[0] + 1
        df.loc[currently_washing, 'BW StepNo'] = step_no_active

    return df

def postprocess_df_elv(df, reference_time):
    df['Bed Volumes Treated'] = df['Total Flow Since Regen'] /155

    def adapt_service_status(x):
        if x == 'ON':
            return 'IN SERVICE'
        elif x == 'OFF':
            return 'OUT OF SERVICE'
        else:
            return x
    df['Service Status'] = df['Service Status'].apply(lambda x: adapt_service_status(x))

    df.loc[df['BW StepNo']> 0, 'Service Status'] = 'WASHING'

    return df

def postprocess_df_acomb(df, reference_time):

    df['Service Status']= 'IN SERVICE'
    df['BW StepNo'] = df['BW StepNo'].astype(int)
    out_ser_indeces = df.loc[df['BW StepNo'].isin([0,16])].index.values
    washing_indeces = df.loc[df['BW StepNo'].isin(np.arange(2,13))].index.values
    df.loc[out_ser_indeces, 'Service Status'] = 'OUT OF SERVICE'
    df.loc[washing_indeces, 'Service Status'] = 'WASHING'

    return df


preproc_oos_funcs = {}
preproc_bw_funcs = {
        'ACOMB': preproc_bw_current_acomb,
        'HUBY': preproc_bw_current_huby,
        'ELVINGTON': preproc_bw_current_elv,
}
preproc_bwdue_funcs = {
        'ACOMB': preproc_bwdue_acomb,
        'HUBY':preproc_bwdue_huby,
        'ELVINGTON':preproc_bwdue_elv,
}

preproc_bwreq_funcs = {
        'ACOMB':preproc_bwreq_acomb,
        'HUBY':preproc_bwreq_huby,
        'ELVINGTON':preproc_bwreq_elv,
}


preproc_filterbed_funcs = {}
preproc_bw_excqueue_funcs = {
        'ACOMB':preproc_exc_queue_acomb,
}

assign_filtering_funcs ={
        'ACOMB':assign_filtering_stat_acomb,
        'HUBY':assign_filtering_stat_huby,
        'ELVINGTON':assign_filtering_stat_elv,
}

preproc_uv254_funcs = {}
preproc_pflow_funcs = {}
preproc_pcounter_funcs = {}
preproc_outflow_funcs = {
        'ELVINGTON':preproc_outflow_elv
}
preproc_inletlevel_funcs = {}
preproc_turbidity_funcs = {}
preproc_bwpressure_funcs = {}
preproc_bwflow_funcs = {}
backwash_req_funcs = {}

postprocess_df_funcs={
        'ACOMB':postprocess_df_acomb,
        'HUBY':postprocess_df_huby,
        'ELVINGTON':postprocess_df_elv
}
