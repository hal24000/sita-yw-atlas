
from data.load_data import colors_risk
import copy
import numpy as np
from datetime import timedelta
from data.collections_object import ProjectCollections
import re
import pandas as pd
from data.data_utils import get_greatest_common_str
from data.summary_utils import query_tags_conditional
import logging
logger = logging.getLogger('rgf_gac_utils')

def variance_within_perc(**kwargs):
    """
    styles a column conditionally based on the normalized deviation within a % threshold
    """
    data_query_dict = []
    header_query_dict = []

    df = kwargs['df']
    col = kwargs['col']
    in_service_col = kwargs['in_service_col']
    out_service_val = kwargs['out_service_val']
    threshold = kwargs['threshold']

    if not isinstance(out_service_val, list):
        out_service_val = [out_service_val]

    mean_val = df.loc[(~df[in_service_col].isin(out_service_val)), col].mean()
    normed_abs_diff_col = 'normed_abs_diff_{}'.format(col)
    df[normed_abs_diff_col] = np.abs(df[col] - mean_val) / mean_val
    formatting_rows = df[(df[normed_abs_diff_col] > threshold) &
                             (~df[in_service_col].isin(out_service_val))].index.values

    hide_column_data, hide_column_header = hide_column(normed_abs_diff_col)

    extra_row_formatting_queries = get_extra_row_formatting(row_indeces=formatting_rows,
                                                            column_id=col,
                                                            color='white',
                                                            backgroundColor=colors_risk['amber'])

    data_query_dict += extra_row_formatting_queries

    data_query_dict += hide_column_data
    header_query_dict += hide_column_header

    return data_query_dict, header_query_dict


def roc_thresh_func(**kwargs):
    """
    FILTER 1 TURBIDITY
    ERGF0101
    >.1 Amber, > .2 Red, Consider Rate of Change
    also use roc thresh as of v 11
    """
    # first get ambers based on ROC
    df_rocs = kwargs['roc_df']
    rocs_thresh = kwargs['roc_thresh']
    col = kwargs['col']
    df = kwargs['df']
    in_service_col = kwargs['in_service_col']
    out_service_vals = kwargs['out_service_vals']

    abs_diff_flag = kwargs.get('abs_diff_flag', False)

    indeces_over = get_indeces_over_roc_2period(df_rocs,
                                                '{}_diff'.format(col),
                                                rocs_thresh,
                                                abs_flag=abs_diff_flag)
    # return data_query_dict, header_query_dict
    revert_indeces = df[df[in_service_col].isin(out_service_vals)].index.values


    format_indeces = set(indeces_over)-set(revert_indeces)
    data_query_dict = get_extra_row_formatting(row_indeces=format_indeces,
                                               column_id='turbidity',
                                               color='white',
                                               backgroundColor=colors_risk['amber'])
    header_query_dict = {}
    # if len(revert_indeces)>0:
    #     extra_formatting = get_extra_row_formatting(column_id=col,
    #                                             backgroundColor=colors_risk['neutral'],
    #                                                 color='black',
    #                                             row_indeces=revert_indeces)
    #
    #     data_query_dict+=extra_formatting
    return data_query_dict, header_query_dict

def abs_value_thresh_based_another(**kwargs):
    col_style = kwargs['col_style']
    col_based = kwargs['col_based']
    thresh = kwargs['thresh']
    df = kwargs['df']
    in_service_col = kwargs['in_service_col']
    out_service_vals = kwargs['out_service_vals']
    amber = thresh['amber']
    row_indeces= df.loc[(df[col_based] > amber['lower']) &
                        (df[col_based] <= amber['upper'])].index.values

    revert_indeces = df[df[in_service_col].isin(out_service_vals)].index.values


    format_indeces = set(row_indeces)-set(revert_indeces)

    row_formatting = get_extra_row_formatting(row_indeces=format_indeces,
                             column_id=col_style,
                             color='white',
                             backgroundColor=colors_risk['amber']

                             )
    if 'red' in thresh:
        red = thresh['red']
        row_indeces = df.loc[df[col_based] > red['lower']].index.values
        row_formatting += get_extra_row_formatting(row_indeces=row_indeces,
                                                  column_id=col_style,
                                                  color='white',
                                                  backgroundColor=colors_risk['red']

                                                  )
    header_query_dict = {}

    # revert_indeces = df[df[in_service_col].isin(out_service_vals)].index.values
    # if len(revert_indeces)>0:
    #     extra_formatting = get_extra_row_formatting(column_id=col_style,
    #                              color='black',
    #                              backgroundColor=colors_risk['neutral'],
    #                              row_indeces=revert_indeces)
    #     row_formatting+=extra_formatting

    return row_formatting, header_query_dict

def abs_value_thresh(**kwargs):
    col = kwargs['col']

    threshold_red = kwargs['red_abs_threshold']
    threshold_amber = kwargs['amber_abs_threshold']
    df = kwargs['df']
    in_service_col = kwargs['in_service_col']
    out_service_vals = kwargs['out_service_vals']

    query_red = '{{{}}} > {} && {{{}}} <= {}'.format(col,
                                                   threshold_red['lower'],
                                                    col,
                                                   threshold_red['upper'])

    query_amber = '{{{}}} > {} && {{{}}} <= {}'.format(col,
                                                   threshold_amber['lower'],
                                                    col,
                                                   threshold_amber['upper'])
    data_query_dict = [
            {'if':{
                    'filter_query': query_red,
                    'column_id': col},
            'backgroundColor': colors_risk['red'],
            'color':            'white'},
            {'if':                      {
                    'column_id':   col,
                    'filter_query': query_amber},
            'backgroundColor': colors_risk['amber'],
            'color':            'white'}

    ]

    revert_indeces = df[df[in_service_col].isin(out_service_vals)].index.values

    if len(revert_indeces)>0:
        extra_formatting = get_extra_row_formatting(column_id=col,
                                                    color='black',
                                                 backgroundColor=colors_risk['neutral'],
                                                 row_indeces=revert_indeces)
        data_query_dict+=extra_formatting

    header_query_dict = {}
    return data_query_dict, header_query_dict

def string_thresh_general(**kwargs):
    """
    """

    col = kwargs['col']
    values = kwargs['values']
    colors = kwargs['colors']

    data_query_dict = []
    for value, color in zip(values, colors):
        data_query_dict += [
                {'if':                      {
                        'column_id':    col,
                        'filter_query': '{{{}}} = "{}"'.format(col, value)},
                'backgroundColor': '{}'.format(colors_risk[color]),
                'color':            'white'}

        ]

    header_query_dict = {}
    return data_query_dict, header_query_dict

def string_thresh(**kwargs):
    """
    """

    col = kwargs['col']
    values_ok = kwargs['val_ok']

    color_alert = kwargs.get('color', 'red')

    if not isinstance(values_ok, list):
        values_ok = [values_ok]

    data_query_dict = [
            {'if':                      {
                    'column_id': col},
                    'backgroundColor': '{}'.format(colors_risk[color_alert]),
                    'color':            'white'},
            {'if':                      {
                    'column_id': col,
                    'row_index':'odd',
                    'filter_query': '{{{}}} = "{}"'.format(col, ProjectCollections.ERROR_MSG_TABLE_RGF_GAC)},
                    'backgroundColor': 'rgb(248, 248, 248)',
                    'color':            'black'},
            {'if':                     {
                    'column_id':    col,
                    'row_index':    'even',
                    'filter_query': '{{{}}} = "{}"'.format(col, ProjectCollections.ERROR_MSG_TABLE_RGF_GAC)},
                    'backgroundColor': 'white',
                    'color':           'black'},

    ]

    for value_ok in values_ok:
        data_query_dict += [{'if':                      {
                'column_id':    col,
                'filter_query': '{{{}}} = "{}"'.format(col, value_ok)},
                'backgroundColor': '{}'.format(colors_risk['green']),
                'color':            'white'}]

    header_query_dict = {}
    return data_query_dict, header_query_dict


def thresh_func(val,thresh):
    """
    get alert color according to specified thresholds
    thresh = {'amber': {'lower': amber[0], 'upper': amber[1]},
              'red': {'lower': red[0], 'upper': red[1]}}
    """


    color = colors_risk['neutral']
    if val >= thresh['amber']['lower'] and val <= thresh['amber']['upper']:
        color = colors_risk['amber']
    elif val >= thresh['red']['lower'] and val < np.inf:
        color = colors_risk['red']

    return color

def thresh_roc_cons(vals, thresholds):
    """
    Consecutive roc values above specified thresholds return an alert
    """
    alert = 1

    for val, threshold in zip(vals, thresholds):
        if threshold < 0:
            if val < threshold:
                alert = alert * 1
            else:
                alert = alert * 0
        else:
            if val > threshold:
                alert = alert * 1
            else:
                alert = alert * 0

    return alert

def get_extra_row_formatting(row_indeces, column_id, color, backgroundColor):
    base_row_formatting_query = {
            'if':              {
                    'row_index': 5,  # number | 'odd' | 'even'
                    'column_id': 'Region'
            },
            'backgroundColor': '',
            'color':           ''
    }
    extra_row_formatting_queries = []
    for row_index in row_indeces:
        base_query = copy.deepcopy(base_row_formatting_query)

        base_query['if']['row_index'] = row_index
        base_query['backgroundColor'] = backgroundColor,
        base_query['color'] = color
        base_query['if']['column_id'] = column_id
        extra_row_formatting_queries.append(base_query)

    return extra_row_formatting_queries


def get_indeces_over_roc_2period(df_rocs, column, thresh, abs_flag=True):
    # get only last 2 periods
    consider_df = df_rocs.loc[df_rocs.index.get_level_values(1).isin(df_rocs.index.get_level_values(1)[-2:])]
    consider_df = consider_df.dropna(subset=[column])

    if not isinstance(thresh, list):
        thresh = np.repeat([thresh],2).tolist()
    if abs_flag:
        consider_df[column] = np.abs(consider_df[column])

    indeces_over = []
    for asset in consider_df.index.get_level_values(0):
        asset_df = consider_df.loc[consider_df.index.get_level_values(0) == asset]
        values = asset_df[column].values
        alert = thresh_roc_cons(values, thresh)
        if alert:
            indeces_over.append(asset)
    # consider_df = consider_df.groupby(level=0).sum().reset_index()
    # indeces_over = consider_df.loc[consider_df[column] >= thresh].index.values
    return indeces_over


def hide_column(column):
    data_query_dict = [
            {'if':      {'column_id': '{}'.format(column)},
             'display': 'None'}
    ]

    header_query_dict = [{'if':      {'column_id': '{}'.format(column)},
                          'display': 'None'}]

    return data_query_dict, header_query_dict


def get_value_diff_periods(reference_time, periods,
                           tags, collection='sensors',
                           index_time = True):
    lookback = (periods+1) * 30
    if not isinstance(tags, list):
        tags = [tags]

    res = []
    for tag in tags:
        temp_res = query_tags_conditional(tag, reference_time=reference_time,
                                     time_span=(lookback / 60) / 24,
                                     collection=collection, latest_only=False)

        try:
            temp_res = temp_res.set_index('measurement_timestamp')
            temp_res['measurement_value'] = temp_res['measurement_value'].astype(float)
        except:
            continue

        temp_res = temp_res.resample('{}'.format('15min'),
                                     label='left').agg({'measurement_value':'mean',
                                                        'tag_name':'first'}).reset_index()
        res.append(temp_res.dropna(axis=0))

    if len(res) == 0:
        return pd.DataFrame()

    res= pd.concat(res,axis=0)


    res = pd.pivot_table(res, index='measurement_timestamp', values='measurement_value', columns='tag_name')


    for col in res.columns:
        res["{}_diff".format(col)] = res[col].diff()

    return res

def filter_description(desc):
    new_desc = re.sub("rgf+\d+", '', desc.lower())
    new_desc = re.sub("gac+\d+", '', new_desc)
    new_desc = re.sub("rgf +\d+", '', new_desc)
    new_desc = re.sub("gac +\d+", '', new_desc)
    new_desc = re.sub("(gac) . ", '', new_desc)
    new_desc = re.sub("gac adsorber+\d+", '', new_desc)
    new_desc = re.sub("gac adsorber +\d+", '', new_desc)
    new_desc = re.sub("no +\d+", '', new_desc)
    new_desc = re.sub("no+\d+", '', new_desc)
    new_desc = re.sub("filter+\d+", '', new_desc)
    new_desc = re.sub("filter +\d+", '', new_desc)
    new_desc = re.sub("\d+", '', new_desc)
    new_desc = re.sub("rgf", '', new_desc)
    new_desc = re.sub("rapid gravity filter no", '', new_desc)
    new_desc = re.sub("rapid gravity filter", '', new_desc)
    new_desc = new_desc.strip(' ')
    return new_desc


def get_individual_tag_df(sites, regex_lists, asset='RGF'):
    site_tag_config = ProjectCollections.site_tag_config
    individual_tag_dfs = []

    for site, taglist in zip(sites,regex_lists):
        individual_tags = {'{}'.format(asset):       [], 'tag_name': [], 'tag_short_description_old': [],
                           'site_name': [], 'tag_short_description': [],
                           'is_alarm':  []}

        # go through the sites specific tags
        for regex_tag in taglist:
            # tags that match regex, should be N where N= number of rgfs. 1 tag for each RGF
            tmp_tags = site_tag_config.loc[site_tag_config.tag_name.str \
                .upper().str.contains(regex_tag,
                                      regex=True)].sort_values(by='scada_tag')
            if len(tmp_tags) == 0:
                continue
            RGFs = np.arange(1, len(tmp_tags) + 1).tolist()
            sites = np.repeat(site, len(tmp_tags)).tolist()
            # print(len(site))
            tmp_descriptions = tmp_tags['tag_short_description'].tolist()
            tmp_descriptions_filtered = []
            for description in tmp_descriptions:

                new_desc = filter_description(description)
                tmp_descriptions_filtered.append(new_desc)

            max_common_description = get_greatest_common_str(tmp_descriptions_filtered)

            # if 'FLOWDEV' in tmp_tags.iloc[0]['tag_name']:
            #     max_common_description=max_common_description+"_stat"

            max_common_descriptions = np.repeat(max_common_description, len(tmp_tags)).tolist()

            individual_tags['{}'.format(asset)] += RGFs
            individual_tags['site_name'] += sites
            individual_tags['tag_name'] += tmp_tags['tag_name'].tolist()

            individual_tags['tag_short_description_old'] += tmp_descriptions

            individual_tags['tag_short_description'] += max_common_descriptions
            individual_tags['is_alarm'] += tmp_tags['is_alarm'].tolist()
            # individual_tags['is_suspect'] += tmp_tags['is_suspect'].tolist()

        individual_tag_dfs.append(pd.DataFrame(individual_tags))

    individual_tag_df = pd.concat(individual_tag_dfs)

    return individual_tag_df


def get_service_status(oos_tags,reference_time, service_func):
    """
    assigns the "filtering" tag to tags that are currently in filtering mode
    service_func, accepts a function, which itself accepts the oos_df with ['measurement_value'] in its columns
    and a reference_time.
    """
    oos_df = query_tags_conditional(oos_tags,
                                    reference_time=reference_time,
                                    latest_only=True)

    oos_df = oos_df.sort_values(by='tag_name').reset_index(drop=True)

    service_stat_df = service_func(oos_df,reference_time)

    return service_stat_df


def update_datastore_df(collection_obj, asset_type,
                        selected_installation, reference_time,
                        postprocess_df_funcs):
    """
    creates the RGF/GAC dataframe for the grid and also the ROCS equivalent df for individual thresholds

    Args:
        collection_obj(GACCollections or RGFCollections): the object holding the necessary metadata
        asset_type(str): the type of asset to query RGF or GAC
    """

    rgf_tags = collection_obj.individual_tag_df
    relevant_tags = rgf_tags.loc[rgf_tags.site_name == selected_installation]
    relevant_tags.set_index(asset_type, inplace=True)

    new_df = []
    rocs_dfs = []
    # relevant_tag_descriptions = relevant_tags.tag_short_description.values

    # style_conditional
    for asset in relevant_tags.index.unique():
        row = {}
        row[asset_type] = asset
        rgf_specific_tags = relevant_tags.loc[asset]
        query_rocs = []
        query_rocs_names = []

        #get all the available columns for this asset at given timestamp
        # e.g.:
        #RGF | column 1 | column 2|
        # 1      1.2        3.4

        for i, rgf_row in rgf_specific_tags.iterrows():

            desc = rgf_row['tag_short_description'].title()
            tag = rgf_row['tag_name']
            isalarm = rgf_row['is_alarm']
            if isalarm:
                collection = 'alarms'
            else:
                collection = 'sensors'


            res = query_tags_conditional(tag_list=tag,
                                         latest_only=True,
                                         time_span=1/24,
                                         reference_time=reference_time,
                                         collection=collection)

            if len(res) != 0:
                row[desc] = res['measurement_value'].values.squeeze()
                if collection == 'sensors':
                    query_rocs.append(tag)
                    query_rocs_names.append(desc)


        #get rocs for the sensor columns
        rocs_res = get_value_diff_periods(reference_time,periods=3,
                                          tags=query_rocs)

        # query_rocs_names += ["{}_diff".format(name) for name in query_rocs_names]
        #set column names to descriptions (same across all assets)
        # rocs_res.columns = query_rocs_names

        #make multi index [Asset num, timestamp]
        asset_id = [asset for z in range(len(rocs_res))]
        prev_index = rocs_res.index
        mux = pd.MultiIndex.from_arrays([asset_id,prev_index], names=[asset_type,
                                                         'measurement_timestamp'])

        # rocs_res['RGF']=asset

        # rocs_res = rocs_res.reset_index().groupby(['RGF','measurement_timestamp']).first()
        rocs_res = rocs_res.set_index(mux, verify_integrity=True)
        # rocs_res['RGF']=

        cols_old = rgf_specific_tags['tag_name']
        cols_new = [tag_spec.title() for tag_spec in rgf_specific_tags['tag_short_description']]
        df_renames = {orig_:rename_ for orig_, rename_ in zip(cols_old, cols_new)}
        df_renames_diff = {orig_+"_diff": rename_+"_diff" for orig_, rename_ in zip(cols_old, cols_new) }
        df_renames.update(df_renames_diff)
        rocs_res.rename(df_renames, axis=1,inplace=True)
        rocs_dfs.append(rocs_res)
        #no data found for any of the columns if only the asset is in the row
        #dont append if so
        if len(row.keys()) > 1:
            new_df.append(row)

    if len(new_df) == 0:
        return pd.DataFrame(), pd.DataFrame()

    new_df = pd.DataFrame(new_df)
    # new_df.to_csv('{}_{}_new_df.csv'.format(asset_type,selected_installation))
    rocs_df = pd.concat(rocs_dfs, axis=0)

    # cols_capped = [col.title() for col in new_df.columns]
    # new_df.columns = cols_capped

    rocs_renames_vanilla = {}
    if selected_installation in collection_obj.col_renames:
        for orig_col, rename_col in collection_obj.col_renames[selected_installation].items():
            try:

                rocs_df = rocs_df.rename({orig_col+"_diff": rename_col+"_diff"},axis=1)
                rocs_df = rocs_df.rename({orig_col:rename_col}, axis=1)
                new_df = new_df.rename({orig_col:rename_col}, axis=1)
            except:
                logger.warning("update_datastore_df: Couldnt rename {} to {}".format(orig_col, rename_col))

    if selected_installation in postprocess_df_funcs:
        """
        Ordering of columns and Keeping metrics together
        RGF, BWStep, Status, Level, Pressure Diff, Flow, Outlet Flow Stat (Flow Status)
        Pressure Diff Needs to be Max Value which will be reached during Backwashing (If we say Max Value over last 24 hours should be sufficient)
        Hours in Filtration, Excess In Service, Excess Time in Queue, Backwash Fail Step,
        Turbidity, Turbidity Low, Turbidity High
        Drop The following Columns
        Excess in Backwash Queue Status, Flow Dev
        """
        try:
            postprocess_df_funcs[selected_installation](new_df,reference_time)
        except:
            pass

    new_order = [col for col in \
                 collection_obj.col_renames[selected_installation].values() if col in new_df.columns]
    new_df = new_df[[asset_type]+new_order]

    for col in new_df.columns:
        #first convert all to string
        try:
            new_df[col] = new_df[col].astype(str)
        except:
            pass
        #then some might be able to be converted to float
        try:
            new_df[col] = new_df[col].astype(float)

        except:
            pass

    new_df = new_df.round(decimals=2)
    new_df.replace("nan", ProjectCollections.ERROR_MSG_TABLE_RGF_GAC,inplace=True)

    return new_df, rocs_df