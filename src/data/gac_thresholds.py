from data.load_data import colors_risk
import copy
import numpy as np
from datetime import timedelta
from data.rgf_collections import RGFCollections
from data.summary_utils import query_tags_conditional
from data.rgf_collections import RGFCollections
from data.rgf_gac_utils import (get_indeces_over_roc_2period,
                                get_extra_row_formatting,
                                hide_column,
                                thresh_func,
                                thresh_roc_cons,
                                abs_value_thresh,
                                string_thresh,
                                roc_thresh_func,
                                variance_within_perc,
                                string_thresh_general,
                                abs_value_thresh_based_another)


bw_threshold ={

}

oos_threshold = {
        'ACOMB': {'amber': {'lower': 1, 'upper': 1},
          'red':   {'lower': 1, 'upper': np.inf}},
        'HUBY':  {'amber': {'lower': 1, 'upper': 1},
          'red':   {'lower': 1, 'upper': np.inf}},
        'ELVINGTON': {'amber': {'lower': 2, 'upper': 2},
                 'red':   {'lower': 2, 'upper': np.inf}},
}

exc_queue_threshold = {
        'ACOMB': {'amber': {'lower': 1, 'upper': 1},
                  'red':   {'lower': 1, 'upper': np.inf}},
        'HUBY':  {'amber': {'lower': 1, 'upper': 1},
                  'red':   {'lower': 1, 'upper': np.inf}},
}

inlet_lvl_thresh = {

        'ACOMB':{'amber': {'lower': 90, 'upper': 95},
                  'red':   {'lower': 95, 'upper': np.inf}},
        'HUBY':{'amber': {'lower': 2.25, 'upper': 2.45},
                  'red':   {'lower': 2.45, 'upper': np.inf}},
        'ELVINGTON':{'amber': {'lower': 90, 'upper': 95},
                  'red':   {'lower': 95, 'upper': np.inf}},
}

turbidity_thresh = {
    "ACOMB": [0.2, 0.2],
    "HUBY": [0.2, 0.2],
    'ELVINGTON':[0.2, 0.2],
}

bw_press_thresh = {

}

bw_flow_thresh = {
    "HUBY": {'amber':{'lower':-np.inf, 'upper': 0},
             'red':{'lower':np.inf, 'upper':np.inf}}
}



def individual_thresholds(installation , df, roc_df, current_datetime):
    base_row_formatting_query = {
            'if': {
                'row_index': 5,  # number | 'odd' | 'even'
                'column_id': 'Region'
            },
            'backgroundColor': '',
            'color': ''
        }

    backwash_filtering_steps = np.arange(3, 13).tolist()
    backwash_filtering_steps += [0] #fail status

    style_cond_queries = []
    style_cond_queries_header = []
    columns = df.columns

    #ELVINGTON threshold configs
    service_stat_args_elv = dict(col = 'Service Status',
                                 values = ['IN SERVICE', 'WASHING','OUT OF SERVICE'],
                                 colors=['green', 'darkgreen','red'],
                                 func = string_thresh_general)
    level_args_elv = dict(col='Level',
                            func = variance_within_perc,
                            threshold = 0.05,
                            in_service_col = 'Service Status',
                            out_service_val = ['OUT OF SERVICE', 'WASHING'])

    outflow_args_elv = dict(col='Outlet Flow',
                                func = variance_within_perc,
                                threshold = 0.05,
                                in_service_col = 'Service Status',
                                out_service_val = ['OUT OF SERVICE', 'WASHING'])

    red_thresholds = {'lower':80, 'upper':10000000}
    amber_thresholds = {'lower':72, 'upper':80}
    filtering_hours_args_elv  = dict(col='Filtering Hours',
                                    func=abs_value_thresh,
                                    in_service_col = 'Service Status',
                                    out_service_vals = ['WASHING','OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)

    pressure_status_args_elv = dict(col='Pressure Status',
                                      val_ok="OFF",
                                      func=string_thresh)

    #===========================================================
    #HUBY thresh args
    service_stat_args_huby = dict(col = 'Service Status',
                                 values = ['IN SERVICE', 'WASHING','OUT OF SERVICE'],
                                 colors=['green', 'darkgreen','red'],
                                 func = string_thresh_general)
    level_args_huby = dict(col='Level',
                                func = variance_within_perc,
                                threshold = 0.05,
                                in_service_col = 'Service Status',
                                out_service_val = ['OUT OF SERVICE', 'WASHING'])
    outflow_args_huby = dict(col='Outlet Flow',
                                func = variance_within_perc,
                                threshold = 0.05,
                                in_service_col = 'Service Status',
                                out_service_val = ['OUT OF SERVICE', 'WASHING'])


    red_thresholds = {'lower':25, 'upper':10000000}
    amber_thresholds = {'lower':20, 'upper':25}

    filtering_hours_args_huby  = dict(col='Filtering Hours',
                                    func=abs_value_thresh,
                                    in_service_col = 'Service Status',
                                    out_service_vals = ['WASHING','OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)

    pressure_diff_args_huby = dict(col='Pressure Diff',
                                      val_ok="OK",
                                      func=string_thresh)

    pressure_status_args_huby = dict(col='Pressure Status',
                                      val_ok="OK",
                                      func=string_thresh)

    outlet_valve_args_huby = dict(col='Outlet Valve Pos',
                                func = variance_within_perc,
                                threshold = 0.05,
                                in_service_col = 'Service Status',
                                out_service_val = ['OUT OF SERVICE', 'WASHING'])

    #=============================================================================
    # ACOMB thresh args
    service_stat_args_acomb= dict(col = 'Service Status',
                                 values = ['IN SERVICE', 'WASHING','OUT OF SERVICE'],
                                 colors=['green', 'darkgreen','red'],
                                 func = string_thresh_general)

    red_thresholds = {'lower':0, 'upper':0}
    amber_thresholds = {'lower':10000000, 'upper':10000000}
    stepno_args_acomb = dict(col='BW StepNo',
                                    func=abs_value_thresh,
                                    in_service_col = 'Service Status',
                                    out_service_vals = ['WASHING','OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)
    level_args_acomb = dict(col='Level',
                                func = variance_within_perc,
                                threshold = 0.05,
                                in_service_col = 'Service Status',
                                out_service_val = ['OUT OF SERVICE', 'WASHING'])

    outflow_args_acomb = dict(col='Outlet Flow',
                                func = variance_within_perc,
                                threshold = 0.05,
                                in_service_col = 'Service Status',
                                out_service_val = ['OUT OF SERVICE', 'WASHING'])

    flow_stat_args_acomb = dict(col='Flow Status',
                                      val_ok="OK",
                                      func=string_thresh)

    red_thresholds = {'lower':68, 'upper':10000000}
    amber_thresholds = {'lower':66, 'upper':68}

    filtering_hours_args_acomb = dict(col='Filtering Hours',
                                    func=abs_value_thresh,
                                    in_service_col = 'Service Status',
                                    out_service_vals = ['WASHING','OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)

    red_thresholds = {'lower':0, 'upper':10000000}
    amber_thresholds = {'lower':10000000, 'upper':10000000}
    excess_time_args_acomb = dict(col='Excess Time(Mins)',
                                    func=abs_value_thresh,
                                    in_service_col = 'Service Status',
                                    out_service_vals = ['WASHING','OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)


    red_thresholds = {'lower':3, 'upper':10000000}
    amber_thresholds = {'lower':2, 'upper':3}
    bw_queue_hrs_args_acomb = dict(col='BW Queue Hrs',
                                    func=abs_value_thresh,
                                    in_service_col = 'Service Status',
                                    out_service_vals = ['WASHING','OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)

    red_thresholds = {'lower':0.3, 'upper':10000000}
    amber_thresholds = {'lower':0.2, 'upper':0.3}

    headloss_args_elv = dict(col='Headloss',
                                    func=abs_value_thresh,
                                    in_service_col = 'Service Status',
                                    out_service_vals = ['WASHING','OUT OF SERVICE'],
                                    red_abs_threshold=red_thresholds,
                                    amber_abs_threshold=amber_thresholds)

    bw_exc_queue_hrs_args_acomb = dict(col='BW Excess Queue Hrs',
                                      val_ok="OK",
                                      color='amber',
                                      func=string_thresh)

    pressure_diff_args_acomb = dict(col = 'Pressure Diff',
                              roc_thresh= 0.15,
                              in_service_col='Service Status',
                              out_service_vals=['WASHING', 'OUT OF SERVICE'],
                              func=roc_thresh_func,
                              abs_diff_flag=False,
                              )

    headloss_args_acomb = dict(col='Headloss',
                                      val_ok="Normal",
                                      func=string_thresh)

    outlet_valve_args_acomb = dict(col='Outlet Valve Pos',
                                func = variance_within_perc,
                                threshold = 0.05,
                                in_service_col = 'Service Status',
                                out_service_val = ['OUT OF SERVICE', 'WASHING'])


    thresh_funcs = {
            'ACOMB':{
                    # "backwash step no": None,
                    'Service Status':service_stat_args_acomb,
                    'BW StepNo':stepno_args_acomb,
                    'Level':level_args_acomb,
                    'Outlet Flow':outflow_args_acomb,
                    'Flow Status':flow_stat_args_acomb,
                    'Filtering Hours': filtering_hours_args_acomb,
                    'Excess Time(Mins)':excess_time_args_acomb,
                    'BW Queue Hrs':bw_queue_hrs_args_acomb,
                    'BW Excess Queue Hrs':bw_exc_queue_hrs_args_acomb,
                    'Pressure Diff':pressure_diff_args_acomb,
                    'Headloss':headloss_args_acomb,
                    'Outlet Valve Pos':outlet_valve_args_acomb,


            },
            "HUBY":{
                    'Service Status':service_stat_args_huby,
                    'Level':level_args_huby,
                    'Outlet Flow':outflow_args_huby,
                    'Filtering Hours':filtering_hours_args_huby,
                    'Pressure Diff':pressure_diff_args_huby,
                    'Pressure Status':pressure_status_args_huby,
                    'Outlet Valve Pos':outlet_valve_args_huby,
            },
            'ELVINGTON':{
                    'Service Status':service_stat_args_elv,
                    'Level':level_args_elv,
                    'Outlet Flow':outflow_args_elv,
                    'Headloss':headloss_args_elv,
                    'Filtering Hours':filtering_hours_args_elv,
                    'Pressure Status':pressure_status_args_elv,

            }

    }



    if installation in thresh_funcs:
        for tag in columns:
            if tag in thresh_funcs[installation]:
                try:
                    args = thresh_funcs[installation][tag]
                    if args is not None:
                        func = args['func']
                        args['df'] = df
                        args['current_datetime'] = current_datetime
                        args['roc_df'] = roc_df
                        args['installation'] = installation
                        style_cond_query, style_cond_query_header = func(**args)
                        style_cond_queries += style_cond_query
                        style_cond_queries_header += style_cond_query_header

                except KeyError:
                    pass

    return style_cond_queries, style_cond_queries_header