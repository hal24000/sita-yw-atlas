import pandas as pd
import numpy as np
from data.collections_object import ProjectCollections
from data.rgf_collections import RGFCollections
from data.summary_utils import query_tags_conditional
from data.rgf_gac_utils import get_service_status
import logging

logger = logging.getLogger('rgf_logger')

def preprocess_rgf_backwashing_acomb(df):
    backwashing = df.loc[(df.measurement_value >= 3) &
                         (df.measurement_value <=12)]
    return len(backwashing)
def preprocess_rgf_backwashing_huby(df):

    backwashing_df = df.loc[df.measurement_value.str.lower() == 'active']
    return_value = len(backwashing_df)

    return return_value

def preprocess_rgf_backwashing_elvington(df):

    backwashing_df = df.loc[df.measurement_value.str.lower() == 'close']
    return_value = len(backwashing_df)

    return return_value

def get_backwash_required_acomb(reference_time):
    headloss_tags = RGFCollections.headloss_alarm_tags['ACOMB']

    headloss = query_tags_conditional(tag_list=headloss_tags, latest_only=True,reference_time=reference_time,
                                           collection='alarms')

    if len(headloss)==0:
        return np.nan, np.nan

    try:
        service_df = get_service_status(oos_tags=RGFCollections.oos_tags_dict['ACOMB'],
                                        reference_time=reference_time,
                                        service_func=assign_filtering_funcs['ACOMB'])

        headloss = headloss.sort_values(by='tag_name').reset_index(drop=True)
        headloss = pd.merge(headloss, service_df, how='left', left_index=True,right_index=True)

    except Exception as e:
        headloss['service_status'] = 'filtering'
        logger.warning('get_backwash_required_acomb: failed checking the filtering status')


    backwash_req = headloss.loc[(headloss.measurement_value.str.lower() == 'high') &
                                 (headloss.service_status == 'filtering')]
    return len(backwash_req) , len(headloss_tags)

def get_backwash_required_huby(reference_time):
    #HUBYWTS1: RGF1_DP_SI DP > 95%

    dp_tags = RGFCollections.differential_pressure_tags['HUBY']

    dp = query_tags_conditional(tag_list=dp_tags, latest_only=True,reference_time=reference_time,
                                               collection='alarms')
    if len(dp)==0:
        return np.nan, np.nan

    try:
        service_df = get_service_status(oos_tags=RGFCollections.oos_tags_dict['HUBY'],
                                        reference_time=reference_time,
                                        service_func=assign_filtering_funcs['HUBY'])

        dp = dp.sort_values(by='tag_name').reset_index(drop=True)
        dp = pd.merge(dp, service_df, how='left', left_index=True,right_index=True)

    except Exception as e:
        dp['service_status'] = 'filtering'
        logger.warning('get_backwash_required_huby: failed checking the filtering status')


    backwash_req = dp.loc[(dp.measurement_value/10000 >= 0.95) &
                          (dp.service_status == 'filtering')]

    return len(backwash_req), len(dp_tags)


def preprocess_outlet_flow_huby(val):
    return val * 0.018


def get_backwash_required_elvington(reference_time):
    filter_bed_cond_tags = RGFCollections.filter_bed_cond_tags['ELVINGTON']
    filterbed = query_tags_conditional(tag_list=filter_bed_cond_tags, latest_only=True,reference_time=reference_time,
                                           collection='sensors')

    if len(filterbed)==0:
        return np.nan, np.nan


    try:
        service_df = get_service_status(oos_tags=RGFCollections.oos_tags_dict['ELVINGTON'],
                                        reference_time=reference_time,
                                        service_func=assign_filtering_funcs['ELVINGTON'])

        filterbed = filterbed.sort_values(by='tag_name').reset_index(drop=True)
        filterbed = pd.merge(filterbed, service_df, how='left', left_index=True,right_index=True)

    except Exception as e:
        filterbed['service_status'] = 'filtering'
        logger.warning('get_backwash_required_elvington: failed checking the filtering status')


    backwas_req = filterbed.loc[(filterbed.measurement_value > 1.5)&
                                (filterbed.service_status == 'filtering')]

    return len(backwas_req), len(filter_bed_cond_tags)

def preprocess_backwash_pressure_acomb(df):
    return df['max_value'].astype(float)


def preprocess_backwash_pressure_elvington(df):
    try:
        return_value = df['measurement_value'].astype(float).values.squeeze()
    except:
        return_value = np.nan
    return return_value

def preprocess_backwash_flow_acomb(val):
    return val

def preprocess_oos_acomb(df):
    oos = df.loc[df.measurement_value.isin(RGFCollections.OOS_VALS['ACOMB'])]
    return oos

def excess_queue_acomb(values):
    n_excess = len(values.loc[(values.measurement_value > 0)&
                              (values.service_status == 'filtering')])

    return n_excess
def excess_queue_huby(values):
    n_excess = len(values.loc[(values.measurement_value > 0)&
                              (values.service_status == 'filtering')])

    return n_excess
def excess_queue_elvington(values):

    n_excess= values.loc[(values.measurement_value != "OK")&
                              (values.service_status == 'filtering')]
    return len(n_excess)


def preprocess_inlet_huby(val):
    if not isinstance(val, float):
        val = float(val)
    return val/10000

def assign_filtering_stat_elv(df,reference_time):
    """
    check both OUT OF SERVICE and bwstenum >0
    """
    df['service_status'] = 'filtering'
    df.loc[df['measurement_value'] == "OUT OF SERVICE", 'service_status'] = 'oos'

    #query the current backwashsteps
    current_bwsteps = query_tags_conditional(tag_list=RGFCollections.backwashing_tags['ELVINGTON'],
                                             reference_time=reference_time,
                                             latest_only=True)
    #sort by name to align units
    current_bwsteps = current_bwsteps.sort_values(by='tag_name').reset_index(drop=True)

    washing = current_bwsteps.loc[current_bwsteps['measurement_value'] == 'CLOSE'].index.values

    df.loc[washing,'service_status'] = 'washing'

    return df['service_status']

def assign_filtering_stat_acomb(df, reference_time):
    """
    acomb only check bwstepnum
    """
    df['service_status'] = 'filtering'
    df.loc[df['measurement_value'].isin([0,16]), 'service_status']= 'oos'
    df.loc[df['measurement_value'].isin(np.arange(2,13)), 'service_status'] = 'washing'

    return df['service_status']

def assign_filtering_stat_huby(df, reference_time):
    """same as gac version, different currently washing tag"""
    df['service_status'] = 'filtering'
    #df contains service status
    df = df.sort_values(by='tag_name') #ensure theyre sorted
    df.loc[df['measurement_value'] == 'OUT OF SERVICE', 'service_status']= 'oos'

    #also check which ones backwashing
    gac_washing_tags = ['HUBYWTS1:RGF_CURRENT_WASHING']
    curr_washing_df = query_tags_conditional(gac_washing_tags,
                                             reference_time=reference_time,
                                             latest_only=True)
    if len(curr_washing_df)>0:
        if int(curr_washing_df['measurement_value']) > 0:
            df.loc[int(curr_washing_df['measurement_value'])-1, 'service_status'] ='washing'
    # df.set_index('tag_name', inplace=True)

    return df['service_status']




backwash_req_funcs={
        'ACOMB':get_backwash_required_acomb,
        'HUBY':get_backwash_required_huby,
        'ELVINGTON':get_backwash_required_elvington
}

preprocess_outlet_flow_funcs = {
        'HUBY': preprocess_outlet_flow_huby
}
excess_queue_funcs = {
        'ACOMB':excess_queue_acomb,
        'HUBY':excess_queue_huby,
        'ELVINGTON':excess_queue_elvington,
}

preprocess_backwashing_funcs ={
        'ACOMB':preprocess_rgf_backwashing_acomb,
        'HUBY':preprocess_rgf_backwashing_huby,
        'ELVINGTON':preprocess_rgf_backwashing_elvington
}

preprocess_inlet_level_funcs = {
        'HUBY':preprocess_inlet_huby
}
preprocess_oos_funcs={
        'ACOMB':preprocess_oos_acomb
}

preprocess_backwash_pressure_funcs = {
        'HUBY': preprocess_backwash_pressure_elvington,
        'ELVINGTON':preprocess_backwash_pressure_elvington,
}
preprocess_backwashflow_funcs = {
        'ACOMB':preprocess_backwash_flow_acomb
}

assign_filtering_funcs = {
        'ACOMB':assign_filtering_stat_acomb,
        'HUBY':assign_filtering_stat_huby,
        'ELVINGTON':assign_filtering_stat_elv,
}


def postprocess_df_acomb(df, reference_time):
    def create_acomb_service_stat(x):
        if x >= 3 and x <= 13:
            return "WASHING"
        elif x in RGFCollections.OOS_VALS['ACOMB']:
            return "OUT OF SERVICE"
        else:
            return "IN SERVICE"

    service_col = RGFCollections.col_renames_acomb['Service Stat']
    bw_stepno_col = RGFCollections.col_renames_acomb['Bw Stepno']

    df[service_col] = df[bw_stepno_col].apply(create_acomb_service_stat)

    #unecessary columns TODO:possibly remove from the collections object
    try:
        df.drop(['Turbidity Flow Low','Turbidity High Status'], axis=1, inplace=True)
    except Exception as e:
        logger.warning('postprocess_df_acomb: error while dropping cols')
        logger.warning(e)

    # df = df[["RGF", "bwstep", Status, Level, Pressure Diff, Flow, Outlet Flow Stat (Flow Status, Hours in Filtration, Excess In Service, Excess Time in Queue, Backwash Fail Step,
    #     Turbidity, Turbidity Low, Turbidity High]]

def postprocess_df_huby(df, reference_time):
    """
    Applies some extra functions/processing on the DF resulting from the RGF table update callback

    for huby we need to add the backwash stepno column using the logic supplied by Francis:

    1. Only 1 RGF can be washing at a time and Tag HUBYWTS1:RGF_CURRENT_WASHING will tell you which one, None Now but Unit was washing earlier
    2. There are nine backwash Steps - Only applicable for Actively Washing Unit (HUBYWTS1:RGFn_WASHING = Active')
        HUBYWTS1:RGF_WASH_STEP_1_ACTIVE…....HUBYWTS1:RGF_WASH_STEP_9_ACTIVE - Check each of these to determine which is Active that will be the Step
    """
    #1. get current washing

    curr_washing_df =  query_tags_conditional(tag_list="HUBYWTS1:RGF_CURRENT_WASHING",
                                           reference_time=reference_time,latest_only=True)
    #2. if curr washing is 0, none are washing, return empty
    df['BW StepNo'] = np.nan
    curr_washing = curr_washing_df['measurement_value'].values[0]
    if curr_washing == 0:
        pass
    else:
        washing_step_tags = ['HUBYWTS1:RGF_WASH_STEP_{}_ACTIVE'.format(i) for i in range(1,10)]
        res = query_tags_conditional(washing_step_tags, latest_only=True, reference_time=reference_time)
        step_no_active = res.loc[res.measurement_value== "Active"].index.values[0]+1
        df.loc[curr_washing-1,'BW StepNo'] = step_no_active

    active_bw_indeces = df.loc[df['BW StepNo'] > 0].index.values
    df.loc[active_bw_indeces, 'Service Status'] = 'WASHING'
    return df


def postprocess_df_elvington(df, reference_time):

    try:
        df.drop(['Turb Inst Warning', 'Turb Inst Failed'], axis=1, inplace=True)
    except Exception as e:
        logger.warning('postprocess_df_elvington: error while dropping cols')
        logger.warning(e)

    currently_washing = df.loc[df['Washing Status'] == 'CLOSE'].index.values
    df.loc[currently_washing, 'Service Status'] = 'WASHING'

    return df

postprocess_df_funcs = {
        'ACOMB':postprocess_df_acomb,
        'HUBY':postprocess_df_huby,
        'ELVINGTON':postprocess_df_elvington
}
