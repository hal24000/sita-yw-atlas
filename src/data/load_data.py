"""
Functions to load dashboard data
"""

import numpy as np
import pandas as pd
from collections import OrderedDict

from .collections_object import ProjectCollections as prj_collections

colors_buttons = {
        'amber':'#FDB515',
        'red':'#E82C2E',
        'neutral':'#FFFFFF ' #white
}

#for the map page
colors_risk_map ={
        'amber':'#FDB515',
        'red':'#E82C2E',
        'neutral':'#00AE84 ' #white
}

colors_risk ={
        'green':'#00AE84',
        'amber':'#FDB515',
        'red':'#E82C2E',
        'darkgreen':'#008261',
        'neutral':'#FFFFFF' #white
}

colors_rag ={
        'amber':'#FDB515',
        'red':'#E82C2E',
        'green':'#2dc937'
}

risk_thresholds = OrderedDict({
        (0,0.4):'neutral',
        (0.4,0.7):'amber',
        (0.7,np.inf): 'red'
})

button_thresholds_daily = OrderedDict({
        (0,0):colors_buttons['neutral'],
        (np.inf,np.inf): colors_buttons['amber'],
        (0,np.inf): colors_buttons['red'],

})
button_thresholds_weekly = OrderedDict({
        (0,np.inf):colors_buttons['neutral'],
        (np.inf,np.inf): colors_buttons['amber'],
        (np.inf,np.inf): colors_buttons['red'],

})
button_thresholds_monthly= OrderedDict({
        (0,np.inf):colors_buttons['neutral'],
        (np.inf,np.inf): colors_buttons['amber'],
        (np.inf,np.inf): colors_buttons['red'],

})

button_thresholds = {
        'daily':button_thresholds_daily,
        'weekly':button_thresholds_weekly,
        'monthly':button_thresholds_monthly
}

limit_df = {
    'Raw Water pH': [6.5, 6.7, 9., 9.5],
    'Clarified Water Turbidity': [-np.inf, -np.inf, 4.5, 5.5],
    'Dosed Raw Water pH (TVAL)': [5.9, 6., 6.8, 6.9],
    'Comb RGF Filtered Water UV254': [-np.inf, -np.inf, 6., 8.],
    'RGF Filtered Water Turbidity': [-np.inf, -np.inf, 0.2, 0.3],
    'Post Bisul Cl Level (TVAL)': [0.35, 0.6, 0.8, 0.9], # not sure what these should be
    'Cont Tank Inlet Free Cl (mg/l)': [0.8, 1., 1.35, 1.9], # what should these be?
    'Comb GAC Filtered Turbidity': [-np.inf, -np.inf, 0.2, 0.4], # unsure about the levels
    'Pre GAC pH (TVAL)': [6.8, 7., 7.9, 8.],
    'Treat Water Alum µg/l': [-np.inf, -np.inf, np.inf, np.inf],
    'Treat Water Cl (Total) (mg/l)': [0.01, 0.02, 0.2, 0.3], # check levels
    'Treat Water Final pH': [6.8, 7., 7.8, 8.2],
    'Treated Water Turbidity': [-np.inf, -np.inf, 0.2, 0.4], # check these levels
}
limit_df = pd.DataFrame(limit_df).T
limit_df.columns = ['LR', 'LA', 'HA', 'HR']

limit_df['LR'] = limit_df['LR'].replace(-np.inf, -99999999)
limit_df['LA'] = limit_df['LA'].replace(-np.inf, -9999999)
limit_df['HA'] = limit_df['HA'].replace(-np.inf,  9999999)
limit_df['HR'] = limit_df['HR'].replace(-np.inf,  99999999)

def load_data(collection, query={}, projection={'_id':0}) -> pd.DataFrame:
    """
    Simple function for loading mongo data into a dataframe.

    Args:
        collection: mongoDB collection
        query: dictionary-like object for query
        projection: dictionary-like object with projection

    Returns:
        pd.DataFrame
    """

    cursor = collection.find(query, projection)
    return pd.DataFrame(list(cursor))


def build_query(
        plant: str,
        metrics: list = list(),
        start_date: pd.Timestamp = None,
        end_date: pd.Timestamp = pd.Timestamp.now(),
        lookback_days: int = None,
        preloaded_data: pd.DataFrame = None,
) -> dict:
    """
    Build query for the database querying.
    """

    end_date = pd.Timestamp(end_date)

    if lookback_days is not None:
        start_date = end_date - pd.Timedelta(days=lookback_days)

    exceptions_per_plant = prj_collections.wq_tag_name_detail_dict.get(plant, dict())

    all_tags = []
    for wq_exception in list(set(exceptions_per_plant.keys()).intersection(set(metrics))):
        all_tags = all_tags + list(exceptions_per_plant.get(wq_exception, list()))

    query = {
        'tag_name': {'$in': all_tags},
        'measurement_timestamp': {'$gte': start_date, '$lte': end_date}
    }

    return query


def load_wq_exceptions_summary(
    plant: str,
    metrics: list = list(),
    start_date: pd.Timestamp = None,
    end_date: pd.Timestamp = pd.Timestamp.now(),
    lookback_days: int = None,
    preloaded_data: pd.DataFrame = None,
) -> pd.DataFrame:
    """
    Pull water quality exception summary from the database.

    Args:
        plant: name of the plant
        metrics: list of the metrics to use
        start_date: from when to count
        end_date: until when  to count
        lookback_days: number of days to look back from end_date, overrides start_date
        preloaded_data (pd.DataFrame): use previously loaded data, overrides the querying

    Returns:
        pd.DataFrame with columns var (name of the exception), issue_type (label of the severity) and n_issues

    TODO:
        add the funcitonal are filter
        remove the all data option
    """

    if not metrics or metrics is None:
        # TODO: get this dynamically
        metrics = ['Raw Water pH',
                   'Clarified Water Turbidity',
                   'Dosed Raw Water pH (TVAL)',
                   'Comb RGF Filtered Water UV254',
                   'RGF Filtered Water Turbidity',
                   'Post Bisul Cl Level (TVAL)',
                   'Cont Tank Inlet Free Cl (mg/l)',
                   'Comb GAC Filtered Turbidity',
                   'Pre GAC pH (TVAL)',
                   'Treat Water Alum µg/l',
                   'Treat Water Cl (Total) (mg/l)',
                   'Treat Water Final pH',
                   'Treated Water Turbidity']

    if preloaded_data is None:
        query = build_query(plant, metrics, start_date, end_date, lookback_days)
        all_exception_data = load_data(prj_collections.series_and_alarms_coll,
                                       query=query)
    else:
        all_exception_data = preloaded_data.copy()

    exceptions_per_plant = prj_collections.wq_tag_name_detail_dict.get(plant, dict())

    issue_df = []
    # loop over all available exceptions
    for wq_exception in list(set(exceptions_per_plant.keys()).intersection(metrics)):
        exception_data = all_exception_data[all_exception_data['tag_name'].map(
            lambda x: x in list(exceptions_per_plant.get(wq_exception, list()))
        )].copy()

        if exception_data.empty:
            for issue_type in limit_df.columns:
                issue_df.append({
                    'var': wq_exception,
                    'issue_type': 'Issue-{}'.format(issue_type),
                    'n_issues': 0
                })
        else:
            exception_data['measurement_value'] = pd.to_numeric(exception_data['measurement_value'],
                                                                errors='coerce')

            # make sure that
            current_limits = [-np.inf] + list(limit_df.loc[wq_exception]) + [np.inf]
            current_limits = sorted(list(set(current_limits)))

            issue_counts = pd.cut(exception_data.loc[exception_data['measurement_value'].map(lambda x: isinstance(x,
                                                                                                                  float)),
                                                     'measurement_value'],
                                  bins=current_limits).value_counts().sort_index().values

            # only take the issues, not "green" periods
            issue_counts = list(issue_counts[:2]) + list(issue_counts[-2:])

            for issue_type, issue_count in zip(list(limit_df.columns)[::-1], issue_counts[::-1]):
                issue_df.append({
                    'var': wq_exception,
                    'issue_type': 'Issue-{}'.format(issue_type),
                    'n_issues': issue_count
                })

    issue_df = pd.DataFrame(issue_df)
    return issue_df
