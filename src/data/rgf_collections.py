# find RGF tags in with regular expressions
## RGF OOS TAGS
from data.collections_object import ProjectCollections
import re
from data.data_utils import get_greatest_common_str
import pandas as pd
import numpy as np
from data.rgf_gac_utils import get_individual_tag_df

class RGFCollections():

    col_renames_acomb = {"Service Stat":'Service Status',
                        "Bw Stepno":'BW StepNo',
                        "Level": "Level",
                        "Outlet Flow":'Outlet Flow', #no rename
                        "Outlet Stat" : "Outlet Flow Stat",
                        "Filtering Hours": 'Filtering Hours',
                        "Excess Time (Mins)": 'Excess Time (Mins)',
                        "Time In Queue":"BW Excess Queue Hrs",
                        "Bw Excess Queue Stat":"BW Excess Queue Stat",
                        "Bw Fail Step": 'BW Fail Step',
                        'Bw Status':"BW Status",
                        "Pressure Diff":"Outlet Pressure",
                        'Pressure Stat':"BW Pressure Exception(Last 24H)",
                        "Turbidity":'Turbidity',
                        'Turbidity High Stat':"Turbidity High Status",#descope
                        'Turbidity Low Stat': "Turbidity Flow Low",#descope
                        'Loss Of Head':"Headloss",
                        "Headloss Status": 'Headloss Status',
                        "Outlet Valve Pos": 'Outlet Valve Pos'
                       }

    col_renames_huby = {
             "In Service Stat":"Service Status",
             'BW StepNo': 'BW StepNo',
             "Level":"Level",
             "Flow":"Outlet Flow",
             "Flow Oor Stat":"Flow OOR Stat",
             "Flow Fault Status":"Flow Fault Stat",
             "Service Time":"Filtering Hours",
             "Fault Status":"RGF Fail Status",
             "Pressure Diff":"Outlet Pressure",
             "Pressure Diff Oor":"Pressure Status",
             "Queued":"BW Queue Status",
             "Wash Queue Position":"BW Queue Position",
             "Washing Stat":"BW Status",
             "Turbidity":"Turbidity",
             "Turbidity Oor":"Turbidity OOR",
             "Outlet Valve Pos":"Outlet Valve Pos",

    }

    col_renames_elvington = {
            "In Service Status":"Service Status",
            "Level(M)":"Level",
            "Outlet Flow":"Outlet Flow",
            "Bed Condition":"Bed Cond",
            "Time In Service":"Filtering Hours",
            "Turbidity":'Turbidity',
            "Turb Inst No Flow":"Turbidity No Flow",
            "Turb Meter Failed":"Turbidity Meter Status",
            "Time Is Washing":"Washing Status",
            "Total Loh (Mbar)":"Headloss",
            "Curr B/Wash Step":"BW StepNo",
            "Time in Queue":"BW Queue Time",
            "Excesstime Inqueue":"BW Queue Alarm",
            "Taken Oos By Hloss":"OOS Headloss",
            "Taken Oos Byhiturb":"OOS Turbidity"
    }

    # col_names_acomb = [ col.lower().title() for col in col_names_acomb]
    # col_names_huby = [ col.lower().title() for col in col_names_huby]
    # col_names_elvington = [ col.lower().title() for col in col_names_elvington]

    col_renames = {'ACOMB': col_renames_acomb,
                 'HUBY':col_renames_huby,
                 'ELVINGTON':col_renames_elvington
                   }
    col_renames_inverted = {'ACOMB': {val: key for key, val in col_renames_acomb.items()},
                             'HUBY':{val: key for key, val in col_renames_huby.items()},
                             'ELVINGTON':{val: key for key, val in col_renames_elvington.items()}
                   }

    DECIM_S = 2
    DECIM_L = 3

    site_tag_config = ProjectCollections.site_tag_config
    BACKWASHING_VALS= {
            'ACOMB': np.arange(3,13).tolist(),
            'HUBY':np.arange(3,13).tolist(),
            'ELVINGTON':np.arange(3,13).tolist()
    }
    BACKWASHING_OOS_VALS = {
            'ACOMB': ["0", 0, 16, "16"],
            'HUBY': ["0", 0, 16, "16"],
            'ELVINGTON': ["0", 0, 16, "16"],

    }
    OOS_VALS = {
            'ACOMB':BACKWASHING_OOS_VALS["ACOMB"],
            'HUBY':['OOS', 'Out of service', 'OUT OF SERVICE'],
            'ELVINGTON':['OOS', 'Out of service', 'OUT OF SERVICE'],
    }

    #"(.*RGF)[1-9]+OUTSER" original
    #(.*RGF_DW_RGF)[1-9]+_BWSTEPNUM" changed to
    accomb_oos_tags = site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF_DW_RGF)[1-9]+_BWSTEPNUM",
                                                                                            regex=True),
                                          "tag_name"].tolist()
    huby_oos_tags = site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+_OOS_",
                                                                                          regex=True),
                                        "tag_name"].tolist()
    elvington_oos_tags = site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*ERGF)(?!00)\d+IOSR",
                                                                                               regex=True),
                                             "tag_name"].tolist()
    oos_tags_dict = {
            'ACOMB':     accomb_oos_tags,
            'HUBY':      huby_oos_tags,
            'ELVINGTON': elvington_oos_tags
    }

    backwashing_tags = {
            'ACOMB':     site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+_BWSTEPNUM",
                                                                                               regex=True),
                                             "tag_name"].tolist(),

            # 'HUBY':      ['HUBYWTS1:RGF_CURRENT_WASHING'],  # only one tag, shows all individuals as an integer sum
            # HUBYWTS1:RGFn_WASHING
            "HUBY": site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+_WASHING",
                                                                                               regex=True),
                                             "tag_name"].tolist(),

            #FILT01 IS WASHING
            'ELVINGTON': site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*ERGF)(?!00)\d+IW",
                                                                                               regex=True),
                                             'tag_name'].tolist()

    }

    service_hour_tags = {
            'ACOMB':     site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+TIQWBWS",
                                                                                               regex=True),
                                             "tag_name"].tolist(),
            'HUBY':      site_tag_config.loc[
                             site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+_SERVICE_TIME_SI",
                                                                               regex=True),
                             'tag_name'].tolist(),
            'ELVINGTON': site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*ERGF)(?!00)\d+TIS",
                                                                                               regex=True),
                                             'tag_name'].tolist()

    }

    filter_bed_cond_tags = {
            'ELVINGTON': site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*ERGF)(?!00)\d+07",
                                                                                               regex=True),
                                             'tag_name'].tolist()
    }

    headloss_alarm_tags = {
            'ACOMB': site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+HDLSSH",
                                                                                           regex=True),
                                         'tag_name'].tolist()
    }

    differential_pressure_tags = {
            'HUBY': site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+_DP_SI",
                                                                                          regex=True),
                                        'tag_name'].tolist()
    }

    excess_queue_tags = {
            'ACOMB':     site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+TIQWBWS",
                                                                                               regex=True),
                                             'tag_name'].tolist(),
            # 'HUBY':site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)[1-9]+HDLSSH",
            #                                                                                    regex=True),
            #                                  'tag_name'].tolist(),
            'ELVINGTON': site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*ERGF)(?!00)\d+ETIQ",
                                                                                               regex=True),
                                             'tag_name'].tolist()
    }

    uv254_tags = {
            'ACOMB':     "ACOMLDS1:CHEM_DW_AT25101SCD",
            'ELVINGTON': 'ELVGTNS1:D_RGF_OL_UV254_SCD'
    }

    particle_flow_tags = {
            'ELVINGTON': 'ELVGTNS1:D_RGF_OL_PARTICLE_FLW'
    }
    particle_counter_tags = {
            'ELVINGTON': 'ELVGTNS1:D_RGF_OL_PARTICLE_SCD'
    }
    outlet_flow_tags = {
            'ACOMB':     'ACOMLDS1:CHEM_DW_FT04X03SUMFLW',
            'HUBY':      'HUBYWTS1:COMBINED_RGF_FLOW_RAW',
            'ELVINGTON': 'ELVGTNS1:FILT_DW_TOTALOF_SCD'
    }

    int_levela_tags = {
            'ELVINGTON':['ELVGTNS1:ERGF0011'],
            'ACOMB':['ACOMLDS1:RGF_DW_LT04001SCD'],
            'HUBY':['HUBYWTS1:RGF_INLET_CHANNEL'],
    }
    int_levelb_tags = {
            'ELVINGTON':['ELVGTNS1:ERGF0012'],
    }
    turbiditya_tags = {
            'ACOMB':['ACOMLDS1:CHEM_DW_AT25201SCD'],
            'HUBY':['HUBYWTS1:RGF_COMMON_TB_SI'],
            'ELVINGTON':['ELVGTNS1:ERGF000A'],
    }

    turbidityb_tags = {
            'ELVINGTON':['ELVGTNS1:ERGF000B'],
    }

    backwash_pressure_a_tags ={
          #   outlet pressure alarm: ACOMLDS1:RGF_A_PS04104H|  sensor: pressure diff m: ACOMLDS1:RGF_DW_PT04102SCD
            #"(.*RGF)+_DW_PT04[1-9]+02SCD"
            'ACOMB':site_tag_config.loc[site_tag_config.tag_name.str.upper().str.contains("(.*RGF)+_DW_PT04[1-9]+02SCD",
                                                                                           regex=True),
                                         'tag_name'].tolist(),

            'HUBY':['HUBYWTS1:PT5000_PV'],
            'ELVINGTON':['ELVGTNS1:ERGFBWP1'],
    }
    backwash_pressure_b_tags ={
            'ELVINGTON':['ELVGTNS1:ERGFBWP2']
    }
    backwash_flow_tags = {
            'ACOMB':['ACOMLDS1:DAF_DW_FT05409SCD'],
            'HUBY':['HUBYWTS1:RGF_BKWASH_FLOW_SI'],
            'ELVINGTON':['ELVGTNS1:ERGF0003'],
    }


    individual_regextags_acomb = [
    "(.*RGF)[1-9]+OUTSER$",
    "(.*DW_FT04)[1-9]+03SCD$",
    "(.*DW_PT04)[1-9]+02SCD$",
    "(.*RGF)[1-9]+TIFWBWS$",
    "(.*RGF)[1-9]+TIQWBWS",
    "(.*RGF)[1-9]+LOHWBWS",
    "(.*RGF)[1-9]+EXTBQ$",
    "(.*RGF)[1-9]+TIQWBWS$",
    "(.*RGF)[1-9]+_EXCESSTIME$",
    "(.*RGF)[1-9]+FLOWDEV$",
    "(.*RGF)[1-9]+_BWSTEPNUM$",
    "(.*RGF)[1-9]+_BWFAILSTEP$",
    "(.*RGF)[1-9]+BWFA$",
    "(.*RGF)[1-9]+LOWFLOWA$",
    "(.*LT04)[1-9]+01SCD$",
    "(.*LT04)[1-9]+02SCD$",
    "(.*DW_AT24)[1-9]+01SCD$",
    "(.*AT24)[1-9]+01H$",
    "(.*A_FS24)[1-9]+02L$",
    "(.*RGF)[1-9]+HDLSSH$",
    "(.*PS04)[1-9]+04H$",
    "(.*RGF_DW_V04)[1-9]+05POS$",
    ]


    individual_regextags_huby = [
    "(.*RGF)[1-9]+_OOS_INSERV_SP$",
    "(.*RGF)[1-9]+_FLOW_SI$",
    "(.*RGF)[1-9]+_LEVEL_SI$",
    "(.*RGF)[1-9]+_TB_SI$",
    "(.*RGF)[1-9]+_DP_SI$",
    "(.*RGF)[1-9]+_DP_OOR$",
    "(.*RGF)[1-9]+_FAULT$",
    "(.*RGF)[1-9]+_FLOW_FAULT$",
    "(.*RGF)[1-9]+_FLOW_OOR$",
    "(.*RGF)[1-9]+_SERVICE_TIME_SI$",
    "(.*RGF_QUEUE_POS_)[1-9]$",
    "(.*RGF)[1-9]+_QUEUED$",
    "(.*RGF)[1-9]+_TB_OOR$",
    "(.*RGF)[1-9]+_WASHING$",
    "(.*RGF)[1-9]+_TB_FAILED$",
    "(.*RGF)[1-9]+_OUTLET_VLV_POS_SI$",

    ]
    individual_regextags_elvington = [
    "(.*ERGF)(?!00)\d+07$",
    "(.*ERGF)(?!00)\d+IOSR$",
    "(.*ERGF)(?!00)\d+05$",
    "(.*ERGF)(?!00)\d+02$",
    "(.*ERGF)(?!00)\d+TIS$",
    "(.*ERGF)(?!00)\d+IW$",
    "(.*ERGF)(?!00)\d+03$",
    "(.*ERGF)(?!00)\d+01$",
    "(.*ERGF)(?!00)\d+CBS$",
    "(.*ERGF)(?!00)\d+TIQ$",
    "(.*ERGF)(?!00)\d+67$",
    "(.*ERGF)(?!00)\d+67_2$",
    "(.*ERGF)(?!00)\d+67_3$",
    "(.*ERGF)(?!00)\d+ETIQ$",
    "(.*ERGF)(?!00)\d+TMF$",
    "(.*ERGF)(?!00)\d+TOOSBH$",
    "(.*ERGF)(?!00)\d+TOOSHT$",
    ]

    individual_tag_df = get_individual_tag_df(['ACOMB',
                                               'HUBY',
                                               'ELVINGTON'],
                                              [individual_regextags_acomb,
                                               individual_regextags_huby,
                                               individual_regextags_elvington])


    num_rgfs = individual_tag_df.groupby(['RGF', 'site_name']).sum().reset_index()
    num_rgfs['count'] = 1
    num_rgfs = num_rgfs.groupby("site_name")['count'].sum()
    individual_tag_df.to_csv("individual_rgf_tags.csv")
