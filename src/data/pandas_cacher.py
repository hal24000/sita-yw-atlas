import pandas as pd
import time
import gc
import threading
from brain_plasma import Brain
from dash.dependencies import Input, Output, State
import dash
from dash.exceptions import PreventUpdate
import hashlib
from data.project_loggers import dash_logger
import logging
logger = logging.getLogger(dash_logger)
from data.collections_object import ProjectCollections
import json
import copy
from datetime import datetime, timedelta


def hash_query(query):
    """
    Changes the date components of the query to strings so that a hash of the query can be computed
    """
    query_to_hash = copy.deepcopy(query)
    for timestamp_column in ['measurement_timestamp', 'bsc_start', 'actual_release']:
        if timestamp_column in query:
            if "$lte" in query[timestamp_column]:
                ts_tmp = query[timestamp_column]['$lte']
                end_date_string = datetime.strftime(ts_tmp, '%Y-%m-%d %H:%M')
                query_to_hash[timestamp_column]['$lte'] = end_date_string
            if "$gte" in query[timestamp_column]:
                ts_tmp = query[timestamp_column]['$lte']
                start_date_string = datetime.strftime(ts_tmp, '%Y-%m-%d %H:%M')
                query_to_hash[timestamp_column]['$gte'] = start_date_string

    hashed_query = hashlib.sha1(json.dumps(query_to_hash).encode()).hexdigest()
    return hashed_query

def query_data(timestamp):
    logger.warning("querying: {}".format(timestamp))
    acomb_oos_tags = ['ACOMLDS1:RGF_A_RGF1OUTSER', 'ACOMLDS1:RGF_A_RGF2OUTSER', 'ACOMLDS1:RGF_A_RGF3OUTSER',
                      'ACOMLDS1:RGF_A_RGF4OUTSER', 'ACOMLDS1:RGF_A_RGF5OUTSER', 'ACOMLDS1:RGF_A_RGF6OUTSER']

    collection = ProjectCollections.alarms_coll

    end_date_string = datetime.strftime(timestamp, '%Y-%m-%d %H:%M')
    start_date_string = (timestamp - timedelta(days=1)).strftime( '%Y-%m-%d %H:%M')

    end_date_dt = pd.to_datetime(end_date_string)
    start_date_dt = pd.to_datetime(start_date_string)
    start_date_dt = pd.to_datetime(start_date_string)

    query = {
            "tag_name":              {"$in": acomb_oos_tags},
            "measurement_timestamp": {"$lte": end_date_dt,
                                      '$gte': start_date_dt}}


    query_to_hash = copy.deepcopy(query)
    query_to_hash['measurement_timestamp']['$lte'] = end_date_string
    query_to_hash['measurement_timestamp']['$gte'] = start_date_string

    query_hash = hashlib.sha1(json.dumps(query_to_hash).encode()).hexdigest()

    res = pd.DataFrame(list(collection.find(query)))

    return res, query_hash


class BrainHolder(object):
    def __init__(self, client_brains, namespace,
                 keep_period=1060, cleanupfreq = 120):
        """
        keep_period is in minuntes
        cleanup freq in seconds
        """
        # super(PandasCacher, cls).__init__()
        self.keep_period = keep_period
        self.cleanup_freq = cleanupfreq
        self.namespace=namespace
        self.clients = {}
        for client_brain in client_brains:
            brain = Brain(namespace=namespace)
            self.clients[client_brain] = brain
        self.stop = False

        if "cleanup" in client_brains:
            thread = threading.Thread(target=self.cleanup, args=())
            thread.daemon = True  # Daemonize thread
            thread.start()

    def cleanup(self):
        ###
        while not self.stop:
            hash_keys = self.clients['cleanup'].names(namespace=self.namespace)
            remove_hashes = []
            for hash_key in hash_keys:
                time_alive = datetime.now() - self.clients['cleanup'][hash_key]['time_created']
                time_alive = time_alive.total_seconds()
                if time_alive>=self.keep_period:
                    remove_hashes.append(hash_key)

            if len(remove_hashes) > 0:
                for key in remove_hashes:
                    del self.clients['cleanup'][key]
            gc.collect()
            time.sleep(self.cleanup_freq)

    def get(self, client_id, key):
        try:
            data = self.clients[client_id][key]['frame']
        except:
            data = pd.DataFrame()
        return data

    def delete(self,client_id, key):
        success = False
        try:
            del(self.clients[client_id][key])
            success = True
        except KeyError:
            pass
        return success

    def store(self,client_id, key, frame, time_created):
        self.clients[client_id][key] = {'frame':frame, 'time_created':time_created}
        # cls.brain[key] = frame

#
# def create_callback_store_value_brain(app: dash.Dash,
#                                    cacher:BrainHolder):
#     @app.callback(Output('datastore-latest', 'data'),
#                   [Input('data-fetch-bttn', 'n_clicks')])
#     def store_value(n_clicks):
#         # when the app starts all callbacks are triggered by default.
#         # raise a PreventUpdate to avoid the callback trigger at start (n_clicks is 0 at this point)
#         if n_clicks is None:
#             raise PreventUpdate
#
#         ts = datetime.now()
#         data, key = query_data(ts)
#         cacher.store('store1',key,data,ts)
#
#         end = datetime.now()
#         logger.warning(" Time taken to query: {}".format(end - ts))
#         return key
#
# def create_value_retrieval_brain(app: dash.Dash,
#                             cacher: BrainHolder,
#                            output:str):
#     @app.callback(output=Output(output, 'children'), inputs=[Input('datastore-latest', 'data')])
#     def update_dummyoutput(query_hash):
#
#         if query_hash is None:
#             return []
#         data = cacher.get(output, query_hash)
#         # brain.object_id()
#         return len(data)
