from visualization.index import app
from argparse import ArgumentParser
water_app = app
server = water_app.server

if __name__ == "__main__":
    argparser = ArgumentParser()
    argparser.add_argument("-local_mode", action="store_true",
                           default=False, required=False)
    argparser.add_argument("-remote_debugger", action="store_true",
                           required=False, default=True)

    args = argparser.parse_args()

    water_app.run_server(debug=True)
